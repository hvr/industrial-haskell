<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>VU 183.653 [2020] - Industrial Haskell - 3rd Unit</title>

	<meta name="description" content="Lecture Notes for Industrial Haskell Course">
	<meta name="author" content="Herbert Valerio Riedel">

	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">

	<link rel="stylesheet" href="../reveal.js/css/reveal.css">
	<link rel="stylesheet" href="../reveal.js/css/theme/solarized.css" id="theme">

	<!-- Code syntax highlighting -->
	<link rel="stylesheet" href="../reveal.js/lib/css/zenburn.css">

	<!-- Printing and PDF exports -->
	<script>
		var link = document.createElement( 'link' );
		link.rel = 'stylesheet';
		link.type = 'text/css';
		link.href = window.location.search.match( /print-pdf/gi ) ? '../reveal.js/css/print/pdf.css' : '../reveal.js/css/print/paper.css';
		document.getElementsByTagName( 'head' )[0].appendChild( link );
	</script>

        <style type="text/css">
          p { text-align: left; }
        </style>
</head>

<body>
  <div class="reveal">
    <div class="slides">
      <section>
        <h2><a href="https://tiss.tuwien.ac.at/course/courseDetails.xhtml?courseNr=183653">VU 183.653</a></h2>

        <h2><small>Methodical,</small> <span style="color: #268bd2;">Industrial</span> <small>Software-Engineering using the</small> <span style="color: #268bd2;">Haskell</span> <small>Functional Programming Language</small></h2>
        <p style="text-align: center;">3<sup>rd</sup> Unit</p>
      </section>


<section data-markdown data-separator="^\n---\n$" data-separator-vertical="^\n--\n$">
  <script type="text/template">

## Today

 - Concurrent Haskell

 - Parallel Haskell

 - Inter-Thread Communication & State

---

## Parallelism vs. Concurrency

 - **Concurrency** = ***dealing*** with lots of things at once
    - Allow **interleaved execution** of multiple tasks
    - Useful for dealing with I/O
    - Affects program structure

--

## Parallelism vs. Concurrency

 - **Parallelism** = ***doing*** lots of things at once
    - **Simultaneous** execution of multiple tasks
    - Useful for **speeding up** CPU bound computations
    - Requires multiple execution units (→ multicore).

---

### Process | Threads | Green-Threads

 - **Process**: executed instance of a program including its state
    - Heavy-weight due to context(-switches)

--

### Process | Threads | Green-Threads

 - **(OS-)Thread**: (aka *light-weight process*) execution thread
     inside *process* scheduled by OS
    - Slightly less heavy-weight context(switches) between threads due to shared context

--

### Process | Threads | Green-Threads

 - **Green-Threads**: (aka *microthreads* or *user-space threads*) *really* light-weight threads
     scheduled by process itself
    - Significantly less overhead (& context) compared to OS-threads

--

### Process | Threads | Green-Threads

 - ...and here's what GHC's `-threaded` runtime does:

   [![](threads.png)](http://www.well-typed.com/blog/53/)

---

## Concurrent Haskell

 - Provides means to create interleaved threads of execution.

 - Most important new primitive (from [`Control.Concurrent`](https://hackage.haskell.org/package/base/docs/Control-Concurrent.html)):
 
    ```haskell
    forkIO :: IO () -> IO ThreadId
    ```

    `forkIO` spawns a **new thread** for executing the given ` IO ()` action.

    - The `ThreadId` handle provides a way to identify the new thread for
      throwing asynchronous exceptions to. We'll get back to that in a minute.

--

## Concurrent Haskell

 - A very ~~stupid~~simple **single-threaded** HTTP server:

```haskell
main = withSocketsDo $ do
    sock <- listenOn $ PortNumber 8000
    loop sock
  where
    loop sock = do
        (h,_,_) <- accept sock
        body h
        loop sock

    body h = hPutStr h msg >> hFlush h >> hClose h

    msg = "HTTP/1.0 200 OK\r\nContent-Length: 7\r\n\r\nPong!\r\n"
```

  - *where could `bracket` and friends be put to work in the code above?*

--

## Concurrent Haskell

 - A very ~~stupid~~simple **concurrent** HTTP server:

```haskell
main = withSocketsDo $ do
    sock <- listenOn $ PortNumber 8000
    loop sock
  where
    loop sock = do
        (h,_,_) <- accept sock
        forkIO $ body h
        loop sock

    body h = hPutStr h msg >> hFlush h >> hClose h

    msg = "HTTP/1.0 200 OK\r\nContent-Length: 7\r\n\r\nPong!\r\n"
```

--

## Concurrent Haskell

Sample project: place the 3 files into a folder

 - [`netserver/cabal.project`](./netserver/cabal.project)
 - [`netserver/netserver.cabal`](./netserver/netserver.cabal)
 - [`netserver/server1.hs`](./netserver/server1.hs)

Then build and run simply by invoking

```
cabal update
cabal run
```

---

## Asynchronous Exceptions

Haskell exceptions were already introduced in the 2<sup>nd</sup> unit:

```haskell
data SomeException = ∀e . Exception e => SomeException e

class (Typeable e, Show e) => Exception e where
    toException   :: e -> SomeException
    fromException :: SomeException -> Maybe e


throwIO :: Exception e => e -> IO a
throw   :: Exception e => e -> a
catch   :: Exception e => IO a -> (e -> IO a) -> IO a
try     :: Exception e => IO a -> IO (Either e a)
```

--

## Asynchronous Exceptions

  - Exceptions thrown via `throw` are considered **synchronous**, as
    they occur explictly at well-defined points in the control flow.
    
  - However, there are also **asynchronous** exception such as
    `StackOverflow` or `HeapOverflow`, which can potentially occur "at
    any point" during execution.

--

## Asynchronous Exceptions

  - Especially relevant for concurrent programming,
    **Asynchronous** exceptions can be thrown by user-code via
  
    ```haskell
    throwTo :: ThreadId -> Exception -> IO ()
    
    killThread :: ThreadId -> IO ()
    killThread tid = throwTo tid ThreadKilled
    ```

--

## Asynchronous Exceptions

  - `throwTo` returns normally only after the exception has been delivered to the target.
  
  - Critical section can be protected from asynchronous exceptions by `mask`:
  
    ```haskell
    bracket acquire release action = mask $ \restore -> do
        h <- acquire
        r <- restore (action h) `onException` release h
        _ <- release h
        return r
    ```

--

## Asynchronous Exceptions

  - However, some (usually) blocking operations are **interruptible**
    which means they can receive asynchronous exception even in the
    scope of a `mask`!

---

## [The Software Crises](http://en.wikipedia.org/wiki/Software_crisis)

> <small>The major cause of the software crisis is that the machines have become several orders of magnitude more powerful! To put it quite bluntly: as long as there were no machines, programming was no problem at all; when we had a few weak computers, programming became a mild problem, and now we have gigantic computers, programming has become an equally gigantic problem.</small>
>
>  <small>— Edsger Dijkstra, The Humble Programmer (EWD340)</small>

--

## The Software Crises

 - The 1<sup>st</sup> Software Crisis: Assembly languages didn't scale

 - The 2<sup>nd</sup> Software Crisis: Imperative/procedural languages (& tools) didn't scale

 - The 3<sup>rd</sup> Software Crisis: OO languages don't scale?

--

## The Software Crises

 - The 3<sup>rd</sup> Software Crisis: OO languages don't scale
    - CPU manufacturers are hitting a wall **increasing clock frequencies** and turn to **increasing number of cores** instead to provide more performance
    - Automatic parallelization (even in a pure language) is a hard problem (→ ongoing research)
    - Need better languages & tools for exploiting multicore processors

--

## The Software Crises

<small>          
> I think the lack of reusability comes in object-oriented languages,
not functional languages. Because the problem with object-oriented
languages is they’ve got all this implicit environment that they carry
around with them.  
> **You wanted a banana but what you got was a gorilla
holding the banana and the entire jungle.**
>
> If you have referentially transparent code, if you have pure
functions — all the data comes in its input arguments and everything
goes out and leave no state behind — it’s incredibly reusable.
> <tt>(Source: Joe Armstrong in ["Coders at Work"](https://books.google.at/books?id=nneBa6-mWfgC&lpg=PA205&pg=PA205#v=onepage&q&f=false))</tt>
</small>

--

## The Software Crises
          
- Edsger Dijkstra once said: <q>“Object-oriented programming is an exceptionally bad idea which could only have originated in California.”</q>

  The following lengthy "opinion piece" hints at some of the issues:

  [Object Oriented Programming is an expensive disaster which must end | Smash Company](http://www.smashcompany.com/technology/object-oriented-programming-is-an-expensive-disaster-which-must-end)

---

### [Amdahl's Law](https://en.wikipedia.org/wiki/Amdahl%27s_law)

 - Speedup `S(N)=1/(1-P+P/N)` with `N` umber of processors and parallel `P` ortion:<br/>
    ![](AmdahlsLaw.svg)

    (source: [Wikimedia Commons](http://commons.wikimedia.org/wiki/File:AmdahlsLaw.svg) / [CC-BY-SA-3.0](http://creativecommons.org/licenses/by-sa/3.0/))

---

## Challenges of Parallelism

  - **Reasoning** about parallel execution is hard

  - Parallelism difficult to **abstract** away

  - Not all Problems are **suited** to be parallelized

  - Different **data-structures** needed sometimes

  - **Debugging** parallel programs is more difficult<br/>
    (non-determinism of bug occurence, combinatorial state-explosion, race conditions, deadlocks, …)

---

## Parallel Haskell

 - Immutable data, purity and explicitness of side-effects reduces freedom of programmer
 
 - C.f. programming constructs providing language support for structured programming

 - Parallel programming still isn't trivial, but it becomes a bit easier with more structure.

 - In Parallel Haskell the language is minimally augmented with a few primitives.

--

## Parallel Haskell

 - Two new primitives for **annotating pure code**:
 
    ```haskell
    infixr 0 `par`
    par :: a -> b -> b
    ```

    ```haskell
    infixr 1 `pseq`
    pseq :: a -> b -> b
    ```

 - `par x y` is semantically equivalent to `y`<br/>
    …but indicates that **WHNF** of `x` *may* be computed in parallel

 - `pseq` is semantically equivalent to `seq`<br/>
   …but with a stronger guarantee w.r.t. execution order

--

## Parallel Haskell

 - Requires linkage with `-threaded` runtime and executaion with
   `+RTS -N2` or more to be of any use

 - `+RTS -N<n>` controls the number of so-called *Haskell Execution
   Contexts* (HEC)

 - Recent GHCs allows to change the number of HECs at runtime via
   ```
   setNumCapabilities :: Int -> IO ()
   ```

 - Threads spawned via `forkIO` are distributed to available HECs and
   thus may exhibit parallelism

--

## Parallel Haskell

  - Which of the following expressions exploits parallelism?

    ```haskell
    x `par` x+y
    x `par` y+x
    x `par` y `par` x+y
    x `par` y `par` y+x
    ```

--

## Parallel Haskell

  - Which of the following expressions exploits parallelism?

    ```haskell
    x `par` x+y
    x `par` y+x
    x `par` y `par` x+y
    x `par` y `par` y+x
    ```

  - Parallelism depends on evaluation order of `(+)`, better to use `pseq` to make evaluation order explicit:

    ```haskell
    x `par` y `pseq` x+y
    ```

--

## Parallel Haskell

  - Obligatory fibonacci-based usage example:

    ```haskell
    fib 0 = 0
    fib 1 = 1
    fib n = f1 + f2
      where
        f1 = fib (n-1)
        f2 = fib (n-2)

    parfib n
      | n < 11    = fib n
      | otherwise = f1 `par` (f2 `pseq` (f1+f2))
      where
        f1 = parfib (n-1)
        f2 = parfib (n-2)
    ```

--

## Parallel Haskell

  - `par` creates **sparks** which may or may not result in a new
    thread
  
  - It's easy to use `par` ineffectively (e.g. by forgetting `pseq`s)
  
  - The previous example falls back to `fib` for *n<11* to avoid
    parallelization overhead
  
  - each HEC has an associated **spark queue**
  
  - idle HECs **steal work** from other HEC's spark queues

--

## Parallel Haskell: `+RTS -s`

 - Spark firing can be monitored via runtime statistics `+RTS -s`:
 
    ```
    SPARKS: 9369514 (215 converted, 1058180 overflowed, 0 dud, 8078160 GC'd, 232959 fizzled)
    ```

     + **converted** sparks mean they were succesfully computed
     + **overflowed** sparks couldn't be created because the spark-queue was full
     + **dud** sparks were not created because they were already evaluated
     + **GC**ed sparks were declared dead before conversion
     + **fizzled** sparks were already evaluated when they were about to be converted

--

### Parallel Haskell: Threadscope

  - Compile with `-rtsopts -eventlog` to enable *runtime event tracing*

  - Run program with the following `+RTS` option:

    ```
    -l[flags]  Log events in binary format to the file <program>.eventlog
               where [flags] can contain:
                  s    scheduler events
                  g    GC and heap events
                  p    par spark events (sampled)
                  f    par spark events (full detail)
                  u    user events (emitted from Haskell code)
                  a    all event classes above
                 -x    disable an event class, for any flag above
               the initial enabled event classes are 'sgpu'
    ```

--

### Parallel Haskell: Threadscope

- Startup up ThreadScope with `threadscope <program>.eventlog` …

![](threadscope.png)

---

## Communication & State

 -  So far we've only discussed independent tasks which didn't interact with each other

 -  Remember `forkIO`'s type signature:

    ```haskell
    forkIO :: IO () -> IO ThreadId
    ```

 -  Knowing the `ThreadId` does not even provide a way to wait on thread completion!\
    (but you can `throwTo` an exception to a `ThreadId`)

--

## Communication & State

 - `IORef`: mutable **lock-free** variables supporting atomic updates

 - `MVar`: **synchronized** mutable `Maybe`-like container

 - `TVar`: software **transactional** memory (STM) variable

---

## `IORef` primitive

 -  `IORef`s are similiar to pointers:

    ```haskell
    data IORef a
    instance Eq (IORef a) -- "pointer equality"
    ```
    ```haskell
    newIORef :: a -> IO (IORef a)
    readIORef :: IORef a -> IO a
    writeIORef :: IORef a -> a -> IO ()
    modifyIORef :: IORef a -> (a -> a) -> IO ()
    modifyIORef' :: IORef a -> (a -> a) -> IO ()
    ```

 - All operations above are **lock-free**, i.e. **no deadlocks** possible.

---

## `IORef` primitive

 -  Important for use in multi-threaded programs

    ```haskell
    atomicModifyIORef' :: IORef a -> (a -> (a, b)) -> IO b
    atomicWriteIORef :: IORef a -> a -> IO ()
    ```

 -  The operations above guarantee consistent write-ordering as seen from other threads ([→ details](http://hackage.haskell.org/packages/archive/base/latest/doc/html/Data-IORef.html#g:2)):

 -  Safe to use `IORef`s if `IORef`s updates have no inter-dependencies, otherwise use `TVar`s.

---

### `MVar` communication primitive

 - Somewhat like a mutable & synchronized `Maybe` container:

    ```haskell
    data MVar a

    newEmptyMVar :: IO (MVar a)
    newMVar      :: a -> IO (MVar a)
    takeMVar     :: MVar a -> IO a
    putMVar      :: MVar a -> a -> IO ()
    ```

  - `takeMVar` takes the `MVar`'s item (but blocks if `MVar` empty)
  - `putMVar` puts an item into the `MVar` (but blocks if `MVar` full)

--

### `MVar` communication primitive

  - Instead of using `takeMVar` and `putMVar` directly, there's are
    useful wrappers which combine `takeMVar`/`putMVar`
    transactions in an atomic (or rather exception-safe) way, e.g.:

    ```haskell
    readMVar   :: MVar a -> IO a
    swapMVar   :: MVar a -> a -> IO a
    withMVar   :: MVar a -> (a -> IO b) -> IO b
    modifyMVar :: MVar a -> (a -> IO (a, b)) -> IO b
    ```

--

### `MVar` communication primitive

```haskell
readMVar   :: MVar a -> IO a
swapMVar   :: MVar a -> a -> IO a
withMVar   :: MVar a -> (a -> IO b) -> IO b
modifyMVar :: MVar a -> (a -> IO (a, b)) -> IO b
```

  - However, care must be taken to avoid issuing `putMVar`s (w/o prior
    `takeMVar`s) while using the wrappers above.

    **→ Don't mix `takeMVar`/`putMVar` with the wrappers listed above.**

--

### `MVar` communication primitive

 - `MVar`s are a versatile primitive that can be used in different ways:

    - As synchronized mutable variables,
    - As channels, with `takeMVar` and `putMVar` as *receive* and *send*, and
    - As a binary semaphore `MVar ()`, with `takeMVar` and `putMVar` as *wait* and *signal*.

 - As opposed to `IORef`s, `MVar`s are not subject to reordering.

---

## Further Reading

 - [Parallel Performance Tuning for Haskell](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.479.9172&rep=rep1&type=pdf)
 - [RWH, Ch. 24: Concurrent and multicore programming](http://book.realworldhaskell.org/read/concurrent-and-multicore-programming.html)
 - [Parallel and Concurrent Programming in Haskell](https://simonmar.github.io/pages/pcph.html)
 - [Debugging Parallel Haskell Programs](http://www.well-typed.com/Chalmers/Presentation.pdf)
 - [Beautiful Concurrency](http://research.microsoft.com/en-us/um/people/simonpj/papers/stm/beautiful.pdf) by SPJ
    
</script>
</section>

	<script src="../reveal.js/lib/js/head.min.js"></script>
	<script src="../reveal.js/js/reveal.js"></script>

	<script>

		// Full list of configuration options available at:
		// https://github.com/hakimel/reveal.js#configuration
		Reveal.initialize({
			controls: true,
			progress: true,
			history: true,
			center: true,

			transition: 'slide', // none/fade/slide/convex/concave/zoom

			// Optional reveal.js plugins
			dependencies: [
				{ src: '../reveal.js/lib/js/classList.js', condition: function() { return !document.body.classList; } },
				{ src: '../reveal.js/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
				{ src: '../reveal.js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
				{ src: '../reveal.js/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
				{ src: '../reveal.js/plugin/zoom-js/zoom.js', async: true },
				{ src: '../reveal.js/plugin/notes/notes.js', async: true }
			]
		});

	</script>

</body>

</html>
