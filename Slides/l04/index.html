<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>VU 183.653 - Industrial Haskell - 4th Unit</title>

	<meta name="description" content="Lecture Notes for Industrial Haskell Course">
	<meta name="author" content="Herbert Valerio Riedel">

	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">

	<link rel="stylesheet" href="../reveal.js/css/reveal.css">
	<link rel="stylesheet" href="../reveal.js/css/theme/solarized.css" id="theme">

	<!-- Code syntax highlighting -->
	<link rel="stylesheet" href="../reveal.js/lib/css/zenburn.css">

	<!-- Printing and PDF exports -->
	<script>
		var link = document.createElement( 'link' );
		link.rel = 'stylesheet';
		link.type = 'text/css';
		link.href = window.location.search.match( /print-pdf/gi ) ? '../reveal.js/css/print/pdf.css' : '../reveal.js/css/print/paper.css';
		document.getElementsByTagName( 'head' )[0].appendChild( link );
	</script>

        <style type="text/css">
          p { text-align: left; }

          .reveal p > code { border: 1px solid; border-radius: 0.15em; padding: 0 0.05em; font-size: 80% }
          .reveal li > code { border: 1px solid; border-radius: 0.15em; padding: 0 0.05em; font-size: 80% }
        </style>
</head>

<body>
  <div class="reveal">
    <div class="slides">
      <section>
        <h2><a href="https://tiss.tuwien.ac.at/course/courseDetails.xhtml?courseNr=183653">VU 183.653</a></h2>

        <h2><small>Methodical,</small> <span style="color: #268bd2;">Industrial</span> <small>Software-Engineering using the</small> <span style="color: #268bd2;">Haskell</span> <small>Functional Programming Language</small></h2>
        <p style="text-align: center;">4<sup>th</sup> Unit</p>
      </section>


<section data-markdown data-separator="^\n---\n$" data-separator-vertical="^\n--\n$">
  <script type="text/template">

# Last Time

 - Concurrent Haskell

 - Parallel Haskell

 - Inter-Thread Communication & State

--

# Today

 - Inter-Thread Communication & State *(cont.)*

 - Parallel strategies & the `Eval` monad

 - `async` package, and `monad-par` package

 - Foreign Function Interface (*FFI*)

---

## Communication & State

 - `IORef`: mutable **lock-free** variables supporting atomic updates

 - `MVar`: **synchronized** mutable `Maybe`-like container

 - `TVar`: software **transactional** memory (STM) variable

---

### `MVar` communication primitive

 - Somewhat like a mutable & synchronized `Maybe` container:

    ```haskell
    data MVar a

    newEmptyMVar :: IO (MVar a)
    newMVar      :: a -> IO (MVar a)
    takeMVar     :: MVar a -> IO a
    putMVar      :: MVar a -> a -> IO ()
    ```

  - `takeMVar` takes the `MVar`'s item (but blocks if `MVar` empty)
  - `putMVar` puts an item into the `MVar` (but blocks if `MVar` full)

--

### `MVar` communication primitive

 - `MVar`s are a versatile primitive that can be used in different ways:

    - As synchronized mutable variables,
    - As channels, with `takeMVar` and `putMVar` as *receive* and *send*, and
    - As a binary semaphore `MVar ()`, with `takeMVar` and `putMVar` as *wait* and *signal*.

 - As opposed to `IORef`s, `MVar`s are not subject to reordering.

---

### Waiting on Thread Completion

 - With `MVar`s we can now wait on thread completion:

    ```haskell
    main = do
        done <- newEmptyMVar
        forkIO $ do
            -- ...do stuff...
            putMVar done ()

        -- ...do other stuff...
        () <- takeMVar done  -- blocks until threads completes
        return ()
    ```

 - Instead of `()` we could also communicate back a result value.

 - Recommended: [`async` package](http://hackage.haskell.org/package/async) (see later slides)

---

### Locks

  - `MVar`s can be used for emulating scoped locks (c.f. binary semaphores):

    ```haskell
    type Lock = MVar ()

    newLock :: IO Lock
    newLock = newMVar ()

    withLock :: Lock -> IO a -> IO a
    withLock x = withMVar x . const
    ```

  - Similar to context-manager syntax in Python:

    ```haskell
    logMsg msg = withLock loggingLock $ do
        print =<< getPOSIXTime
        putStr " | "
        putStrLn msg
    ```

--

### Locks

  - Due to being low-level, **explicit locking** allows for **many**
    potential programming errors:
     - Deadlocks
     - Race conditions
     - Lost wakeups

  - Error handling/rollback in exception handlers requires more
    attention w.r.t. locks.

  - Programming with explicit locks is **hard**

  - Moreover, locks are **non-compositional**

--

### Locks

  - Motivating example in pseudo-code:

    ```java
    class Account{
      double balance;

      synchronized void deposit(double amount) {
        // do some other stuff that may throw exception
        balance += amount;
      }

      synchronized void withdraw(double amount) {
        if (balance < amount)
          throw new OutOfMoneyError();
        balance -= amount;
      }
    }
    ```

 - `synchronized` enforces that at all times, at most one invocation of
   `withdraw()` or `deposit()` in progress per `Account` instance.

--

### Locks

 -  Let's add a `xfer()` method:

```java
class Account{
  /* */

  synchronized void deposit(double amount) { /* */ }
  synchronized void withdraw(double amount) { /* */ }

  synchronized void xfer(Account other, double amount) {
    other.withdraw(amount);
    this.deposit(amount);
  }
}
```

 -  How many potential issues can you spot?

---

### Software Transactional Memory

 - The previous example would be much easier to get right in SQL thanks to **transactions**.

 - Idea for Haskell: provide **atomic blocks** with all-or-nothing semantics and `TVars`

    ```haskell
    atomically :: STM a -> IO a
    ```
    ```haskell
    data TVar a
    newTVar :: a -> STM (TVar a)
    readTVar :: TVar a -> STM a
    ```

 - Haskell's STM provides **atomicity** and **isolation**.

 - STM has been available for Haskell since GHC 6.4.1 (i.e. since 2005)!

--

### Software Transactional Memory

Simple example:

```haskell
withdraw :: TVar Double -> Double -> STM ()
withdraw acc amt = do
    bal <- readTVar acc
    unless (bal >= amt) $
      throwSTM OutOfMoneyError
    writeTVar acc (bal-n)

deposit :: TVar Double -> Double -> STM ()
deposit acc amt = modifyTVar acc (+amt)
```

--

### Software Transactional Memory

 - And now we can safely construct a proper `xfer` implementation:

    ```haskell
    xfer :: TVar Double -> Double -> TVar Double -> STM ()
    xfer srcAcc amt dstAcc = withdraw srcAcc amt >>
                             deposit  dstAcc amt
    main :: IO ()
    main = do
        acc1 <- newTVarIO  900.00 -- 'newTVarIO' similiar to
        acc2 <- newTVarIO  123.45 --  'atomically . newTVar'

        atomically $ do -- same as 'xfer acc1 500.00 acc2'
            withdraw acc1 500.00
            deposit  acc2 500.00

        -- acc1 = 400.00 and acc2 = 623.45
        -- the invariant acc1+acc2 == 1023.45 holds

        Left _ <- try $ atomically $ xfer acc1 500.00 acc2 -- will fail

        -- the invariant acc1+acc2 == 1023.45 holds
    ```

--

### Software Transactional Memory

 - There are other noteworthy STM primitives:

    ```haskell
    retry :: STM a
    orElse :: STM a -> STM a -> STM a
    always :: STM Bool -> STM ()
    ```

 - Warning: `retry` can be used to construct deadlocks!

 - N.b.: Failed/rollbacked transaction = **wasted computation**!
    - Monitor rollback-rate
    - STM may be inappropriate for high contention/rollback-rates

 - This was just a brief overview of STM, but there's plenty of literature!

---

### Summary: IORef/MVar/TVar

 - Provide different features & cost-models

    - `IORef`s are **most lightweight** tool for **atomic updates**

    - `MVar`s provide **synchronization** at the cost of **more overhead** than `IORef`s

    - `TVar`s provide **fast transactions** with **costly rollbacks**.

---

### Parallel Strategies

 - Provided by [`parallel` package](http://hackage.haskell.org/package/parallel)

 - Simple abstraction for specifying the **evaluation degree**:

    ```haskell
    type Strategy a = a -> ()

    using :: a -> Strategy a -> a
    using x s = s x `pseq` x
    ```

 - Some trivial strategies:

    ```haskell
    r0, rseq :: Strategy a
    r0   x = ()
    rseq x = x `pseq` ()

    rdeepseq :: NFData a => Strategy a
    ```

--

## Parallel Strategies

  - A less trivial strategy:

    ```haskell
    parList :: Strategy a -> Strategy [a]
    parList strat []     = ()
    parList strat (x:xs) = strat x `par` parList strat xs
    ```

  - Usage:

    ```haskell
    parMap :: Strategy b -> (a -> b) -> [a] -> [b]
    parMap strat f xs = map f xs `using` parList strat
    ```

    provides parallel mapping with strategy as parameter

--

## Parallel Strategies

 - Strategies exploit lazyness to provide compositionality/modularity

 - Reasoning about performance is obfuscated by exploited lazyness

 - Strategies rely on data-structures with unevaluated components to
   create parallelism

 - However, strategies are just one possible abstraction for providing parallelism

---

## The `Eval` Monad

 - Also provided by [`parallel` package](http://hackage.haskell.org/package/parallel)

 - A more explicit abstraction:

    ```haskell
    data Eval a

    instance Monad Eval

    runEval :: Eval a -> a

    rseq :: a -> Eval a
    rpar :: a -> Eval a
    ```

 - `Eval` is a *strict identity monad*

--

## The `Eval` Monad

 -  Allows to write

    ```haskell
    a `par` (b `pseq` a + b)
    ```

    with a more explicit control flow syntax as

    ```haskell
    runEval $ do
       a' <- rpar a     -- start evaluation of a in parallel
       b' <- rseq b     -- evaluate b
       return $ a' + b' -- return result a+b
    ```

--

## The `Eval` Monad

 - New strategies implementations based on `Eval`:

    ```haskell
    type Strategy a = a -> Eval a

    using :: a -> Strategy a -> a
    using x strat = runEval (strat x)
    ```

 - `rpar` and `rseq` are strategies too:

    ```haskell
    rpar, rseq :: Strategy a
    ```

 - Use `rparWith` to set the evaluation degree of parallel computations:

    ```haskell
    rparWith :: Strategy a -> Strategy a
    ```

---

### `async` Package: Futures & Promises

 - The previous example hints at a common pattern, namely **Futures**

 - The [`async` package](http://hackage.haskell.org/package/async)
   provides ready-to-use (exception-aware) abstractions:

    ```haskell
    data Async a

    async :: IO a -> IO (Async a)
    withAsync :: IO a -> (Async a -> IO b) -> IO b
    wait :: Async a -> IO a
    cancel :: Async a -> IO ()
    ```

 - C.f. `Eval` monad from [`parallel` package](http://hackage.haskell.org/package/parallel)

--

### `async` Package: Convenience Functions

 - The [`async` package](http://hackage.haskell.org/package/async)
   provides other useful helpers as well, e.g.:

    ```haskell
    concurrently :: IO a -> IO b -> IO (a, b)
    mapConcurrently :: Traversable t => (a -> IO b) -> t a -> IO (t b)
    race :: IO a -> IO b -> IO (Either a b)
    ```

 -  *Linking* threads together (w.r.t. cancellation):

    ```haskell
    link :: Async a -> IO ()
    link2 :: Async a -> Async b -> IO ()
    ```

---

### `monad-par` Package: The `Par` Monad

 - The [`monad-par` package](http://hackage.haskell.org/package/monad-par) provides
   an even more explicit abstraction avoiding lazyness

 - The *pure* interface:

    ```haskell
    data Par a
    instance Monad Par

    runPar :: Par a -> a
    fork :: Par () -> Par ()

    data IVar
    new :: Par (IVar a)
    get :: IVar a -> Par a
    put :: NFData a => IVar a -> a -> Par ()
    ```

--

### `monad-par` Package: The `Par` Monad

  - Emphasis on representing computation dataflows:

    ```haskell
    runPar $ do
      [a,b,c,d] <- sequence [new,new,new,new]
      fork $ do x <- get a; put b (x+1)
      fork $ do x <- get a; put c (x+2)
      fork $ do x <- get b; y <- get c; put d (x+y)
      fork $ do put a (3 :: Int)
      get d
    ```

  - `IVar`s can be seen as vertices in a graph and each `IVar` has (at most) one incoming edge.
  - Alternatively, G-style blockdiagrams where `IVar`s represent
    nodes' inputs/outputs and edges correspond to `get`/`put` pairs.

--

### `monad-par` Package: The `Par` Monad

 - Convenience operation `spawn` providing future/promise pattern:

    ```haskell
    spawn :: NFData a => Par a -> Par (IVar a)
    spawn p = do
        r <- new
        fork (p >>= put r)
        return r
    ```

--

### `monad-par` Package: The `Par` Monad

 - Parallel Fibonacci example, again:

    ```haskell
    parfib :: Int -> Par Int
    parfib n
      | n <= 2     = return 1
      | otherwise  = do
         f1 <- spawn $ parfib (n - 1)
         f2 <- spawn $ parfib (n - 2)
         f1' <- get f1
         f2' <- get f2
         return (f1' + f2')
    ```

---

### Foreign function interface (FFI)

 - Sometimes we want/need to interface with non-Haskell code, e.g. for
    - System calls
    - Platform-optimized code
    - Existing proprietary libraries
    - Existing non-proprietary libraries

 - In H98 FFI is an addendum.

 - In H2010 FFI is part of the language.

 - Currently, only a **FFI for C** is properly implemented.

--

### Foreign function interface (FFI)

 - Haskell's FFI provides facilities to **call C function from Haskell** code.

 - Allows to to **call Haskell functions from C** code (not covered in this lecture).

 - Works even in GHCi.

--

### FFI: Importing pure functions

```haskell
import Foreign.C -- for CDouble

foreign import ccall unsafe "math.h sin" c_sin :: CDouble -> CDouble

sin :: Double -> Double
sin = realToFrac . c_sin . realToFrac
```

 - `ccall` defines c calling convention (there's `stdcall` for Win32).

 - `unsafe` specifies that the called function won't call back into Haskell.

 - `CDouble` is a foreign type which corresponds exactly to C's `double` type.

--

### FFI: Importing pure functions

 - By convention, FFI imported function are named with a "`c_`" prefix.

 - Function pointers (imported by declaring `"math.h &sin"`) are
   conventionally prefixed with "`p_`".

 - The specified header `math.h` is optional (and currently ignored by
   GHC for the `ccall` convention).

 - Since GHC 7.6.1: New `capi` calling convention which really makes use of the C header file
   (required if the imported C function is actually CPP mangled or `inline`d)

--

### FFI: Importing impure functions

  - What's wrong with

    ```haskell
    foreign import ccall unsafe "stdlib.h rand" c_rand :: CUInt
    ```

    ?

--

### FFI: Importing impure functions

  - What's wrong with

    ```haskell
    foreign import ccall unsafe "stdlib.h rand" c_rand :: CUInt
    ```

    ?

  - `rand()` is not a real *function*!

    ![](rand_xkcd.png)

--

### FFI: Importing impure functions

  - Here it's done right:

    ```haskell
    foreign import ccall unsafe "stdlib.h rand"
      c_rand :: IO CUInt

    foreign import ccall unsafe "stdlib.h srand"
      c_srand :: CUInt -> IO ()
    ```

--

### FFI: Importing semi-pure functions

 - Imported C functions which inspect pointers or have internal state
   have to be marked as being impure by putting them in the `IO`
   monad:

    ```haskell
    foreign import ccall unsafe "string.h memchr"
      c_memchr :: Ptr Word8 -> CInt -> CSize -> IO (Ptr Word8)
    ```

 - However, if you can "contain" the side effect, you can use
   `unsafePerformIO` to provide a pure Haskell wrapper.

--

### FFI: Importing semi-pure functions

 - Derived from `Data.ByteString`:

    ```haskell
    foreign import ccall unsafe "string.h memchr"
      c_memchr :: Ptr Word8 -> CInt -> CSize -> IO (Ptr Word8)

    elemIndex :: Word8 -> ByteString -> Maybe Int
    elemIndex c (PS x s l) = unsafePerformIO $ withForeignPtr x $ \p -> do
        let p' = p `plusPtr` s
        q <- c_memchr p' c (fromIntegral l)
        return $! if q == nullPtr then Nothing else Just $! q `minusPtr` p'
    ```

--

### FFI: Managing memory allocations

 - Sometimes C functions **allocate new memory which the caller owns** and has to
   deallocate lateron.

 - But in Haskell computations might be cancelled at any time which
   would lead to **memory leaks**.

> - Need some way to register foreign allocated memory with Haskell's garbage collector.

--

### FFI: Managing memory allocations

 - The `ForeignPtr` facility provides support for integrating with Haskell's GC:

    ```haskell
    type FinalizerPtr a = FunPtr (Ptr a -> IO ())
    newForeignPtr :: FinalizerPtr a -> Ptr a -> IO (ForeignPtr a)
    ```

 - Assume the following stupid C API:

    ```C
    typedef struct foo_s foo_t;

    foo_t *foo_new(int);
    void foo_free(foo_t *);
    ```

    where `foo_new()` constructs (allocates) a new `foo_t` object, and
    `foo_free()` destructs (deallocates) it again.

--

### FFI: Managing memory allocations

 - On the Haskell side, we'd wrap it as follows:

    ```Haskell
    data Foo -- phantom type

    foreign import ccall unsafe "foo_new"
      c_foo_new :: CInt -> IO (Ptr Foo)
    foreign import ccall unsafe "&foo_free"
      p_foo_free :: FunPtr (Ptr Foo -> IO ())

    mkFoo :: Int -> ForeignPtr Foo
    mkFoo i = unsafePerformIO $
              newForeignPtr p_foo_free =<< foo_new (fromIntegral i)
    {-# NOINLINE mkFoo #-}
    ```

 - Note that `mkFoo` provides a pure function API to an impure C API.

--

### FFI: Parallel Haskell

 - `unsafe` FFI calls may block other threads (c.f. non-allocating computations)

 - `safe` FFI calls add about 100 ns overhead:

    ```haskell
    -- int square(int a) { return a*a; }
    foreign import ccall unsafe "square" c_sq_unsafe :: CInt -> CInt
    foreign import ccall   safe "square" c_sq_safe :: CInt -> CInt
    hs_sq x = x*x :: CInt
    ```

    On Intel i7-3770 with GHC 7.6.3/Linux/64bit:
     - about **10 ns** for `hs_sq` and `c_sq_unsafe`
     - about **120 ns** for `c_sq_safe`

 - `safe` FFI calls spawn additional os-threads and don't eat up HECs

---

## Further Reading

 - [Better Strategies for Parallel Haskell](http://community.haskell.org/~simonmar/papers/strategies.pdf)
 - [Parallel Performance Tuning for Haskell](http://community.haskell.org/~simonmar/papers/threadscope.pdf)
 - [RWH, Ch. 24: Concurrent and multicore programming](http://book.realworldhaskell.org/read/concurrent-and-multicore-programming.html)
 - [Parallel Haskell Tutorial: The Par Monad](http://community.haskell.org/~simonmar/slides/CUFP.pdf)
 - [Parallel and Concurrent Programming in Haskell](http://chimera.labs.oreilly.com/books/1230000000929/index.html)
 - [Debugging Parallel Haskell Programs](http://www.well-typed.com/Chalmers/Presentation.pdf)
 - [Haskell and transactional memory](http://research.microsoft.com/en-us/um/people/simonpj/papers/stm/STMTokyoApr10.pdf) Slides by SPJ
 - [Beautiful Concurrency](http://research.microsoft.com/en-us/um/people/simonpj/papers/stm/beautiful.pdf) by SPJ
 - [The Limits of STM](https://www.bscmsrc.eu/sites/default/files/cf-final.pdf)
 - [GHC STM Notes](http://fryguybob.github.io/STM-Commentary/)

--

## More Further Reading

 - [RWH Ch. 17: *Interfacing with C: the FFI*](http://book.realworldhaskell.org/read/interfacing-with-c-the-ffi.html)

 - [Haskell 2010 Lang. Report Ch. 8: FFI](www.haskell.org/onlinereport/haskell2010/haskellch8.html)

 - [Haskell Wiki FFI Page](http://www.haskell.org/haskellwiki/FFI)

</script>
</section>

	<script src="../reveal.js/lib/js/head.min.js"></script>
	<script src="../reveal.js/js/reveal.js"></script>

	<script>

		// Full list of configuration options available at:
		// https://github.com/hakimel/reveal.js#configuration
		Reveal.initialize({
			controls: true,
			progress: true,
			history: true,
			center: true,

			transition: 'slide', // none/fade/slide/convex/concave/zoom

			// Optional reveal.js plugins
			dependencies: [
				{ src: '../reveal.js/lib/js/classList.js', condition: function() { return !document.body.classList; } },
				{ src: '../reveal.js/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
				{ src: '../reveal.js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
				{ src: '../reveal.js/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
				{ src: '../reveal.js/plugin/zoom-js/zoom.js', async: true },
				{ src: '../reveal.js/plugin/notes/notes.js', async: true }
			]
		});

	</script>

</body>

</html>
