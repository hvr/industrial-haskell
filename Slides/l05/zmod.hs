{-# LANGUAGE TypeOperators, DataKinds, KindSignatures, UnicodeSyntax, GADTs #-}

module ZMod where

import GHC.TypeLits

newtype ℤMod (n ∷ Nat) = IM Integer deriving (Eq)

type ZMod = ℤMod -- easier to type

instance (KnownNat n, 1<=n) ⇒ Show (ℤMod n) where
    show im@(IM x) = "(" ++ show x ++ " ∷ ℤ/" ++ (show $ natVal im) ++ ")"

instance KnownNat n ⇒ Num (ℤMod n) where
    (IM x) * (IM y) = fromInteger (x*y)
    (IM x) + (IM y) = fromInteger (x+y)
    (IM x) - (IM y) = fromInteger (x-y)
    signum (IM x)   = fromInteger (signum x)
    abs             = id
    fromInteger i   = let im = IM (i `mod` natVal im) in im
