<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>VU 183.653 - Industrial Haskell - 5th Unit</title>

        <meta name="description" content="Lecture Notes for Industrial Haskell Course">
        <meta name="author" content="Herbert Valerio Riedel">

	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">

	<link rel="stylesheet" href="../reveal.js/css/reveal.css">
	<link rel="stylesheet" href="../reveal.js/css/theme/solarized.css" id="theme">

	<!-- Code syntax highlighting -->
	<link rel="stylesheet" href="../reveal.js/lib/css/zenburn.css">

	<!-- Printing and PDF exports -->
	<script>
		var link = document.createElement( 'link' );
		link.rel = 'stylesheet';
		link.type = 'text/css';
		link.href = window.location.search.match( /print-pdf/gi ) ? '../reveal.js/css/print/pdf.css' : '../reveal.js/css/print/paper.css';
		document.getElementsByTagName( 'head' )[0].appendChild( link );
	</script>

        <style type="text/css">
          p { text-align: left; }
        </style>
</head>

<body>
  <div class="reveal">
    <div class="slides">
      <section>
        <h2><a href="https://tiss.tuwien.ac.at/course/courseDetails.xhtml?courseNr=183653">VU 183.653</a></h2>

        <h2><small>Methodical,</small> <span style="color: #268bd2;">Industrial</span> <small>Software-Engineering using the</small> <span style="color: #268bd2;">Haskell</span> <small>Functional Programming Language</small></h2>
        <p style="text-align: center;">5<sup>th</sup> Unit</p>
      </section>


<section data-markdown data-separator="^\n---\n$" data-separator-vertical="^\n--\n$">
  <script type="text/template">

## Today

 - Code Styling
 - Exhaustive Case Analysis
 - `LambdaCase`
 - Type-level programming
   - Make Illegal States Unrepresentable
   - Phantom Types
   - GADTs
   - `TypeFamilies`
   - `DataKinds`

---

## Code Styling Guidelines

* Following a style guide helps avoiding getting distracted

* Recommended starting point: [Snap Framework's Haskell Style Guide](http://snapframework.com/docs/style-guide)

---

### Benefits of a (Good) Typesystem

* Helps *a lot* with **refactoring**

* Helps **avoiding errors at compile-time**  (c.f. unit tests)

* Provides documentation via type-signatures:

    ```haskell
    ... :: (a -> b) -> [a] -> [b]
    ... :: Monad m => (a -> m Bool) -> [a] -> m [a]
    ```

  (c.f. doc-strings in LISP, Python, et al.)

* Security & Access-control

---

## Exhaustive Case Analysis

* What's the problem with the code below?

```haskell
data CObjKind = CObjPerson | CObjBike | CObjDog

isHuman CObjPerson = True
isHuman _          = False
```

--

## Exhaustive Case Analysis

* More defensive style:

    ```haskell
    data CObjKind = CObjPerson | CObjBike | CObjDog

    isHuman CObjPerson = True
    isHuman CObjBike   = False
    isHuman Cobjdog    = False
    ```

* Use explicit cases to have compiler help you when refactoring `CObjKind`!

---

## `LambdaCase` Intermission

 * Available since GHC 7.6

```haskell
{-# LANGUAGE LambdaCase #-}
data CObjKind = CObjPerson | CObjBike | CObjDog

-- isHuman x = case x of
-- isHuman = \x -> case x of
isHuman = \case
  CObjPerson -> True
  CObjBike   -> False
  Cobjdog    -> False
```

--

## `LambdaCase` Intermission

 * Also particularily useful in monadic context to avoid temporary labels

   ```haskell
   do tmp <- getArgs
      case tmp of
        [] -> ...
        [x] -> ...
   ```

   becomes

   ```haskell
   do getArgs >>= \case
        [] -> ...
        [x] -> ...
   ```

---

### Make Illegal States Unrepresentable

* "A contact shall have either an email or a postal address"

* 1st Attempt:

    ```haskell
    data Contact = Contact
        { cName   :: Name,
        , cEmail  :: EmailInfo,
        , cPostal :: PostalInfo,
        }
    ```

* Can't represent required states ☹

--

### Make Illegal States Unrepresentable

* "A contact shall have either an email or a postal address"

* 2nd Attempt:

    ```haskell
    data Contact = Contact
        { cName   :: Name,
        , cEmail  :: Maybe EmailInfo,
        , cPostal :: Maybe PostalInfo,
        }
    ```

* Can represent required states ☺

--

### Make Illegal States Unrepresentable

* "A contact shall have either an email or a postal address"

* 2nd Attempt:

    ```haskell
    data Contact = Contact
        { cName   :: Name,
        , cEmail  :: Maybe EmailInfo,
        , cPostal :: Maybe PostalInfo,
        }
    ```

* Can represent required states ☺

* …as well as undesirable states ☹

    but what's so bad about the undesired states?<br/>
    (Hint: what's so annoying about nullable types?)

--

### Make Illegal States Unrepresentable

* "A contact shall have **either** an email or a postal address"

* Final Attempt:

    ```haskell
    data Contact = Contact
        { cName           :: Name,
        , cEmailOrPostal  :: Either EmailInfo PostalInfo
        }
    ```

* Can represent **only** required states! ☺

--

### Make Illegal States Unrepresentable

* In languages with _weaker_ typesystems:

    ```C
    typedef struct {
        cobj_kind_t   kind; // person, bike, dog, ...
        cobj_age_t    age;
        cobj_weight_t weight;
        cobj_size_t   size;
        cobj_gender_t gender;
    } cobj_t;
    ```

* Fields exist even though not sensible → error-prone

--

### Make Illegal States Unrepresentable

* With algebraic data types:

    ```haskell
    data CObjKind = CObjPerson Age Gender
                  | CObjBike Weight
                  | CObjDog Size
    ```

* Alternatively (as used in some ML-dialects):

    ```haskell
    data CObjKind = CObjPerson CObjPersonProps
                  | CObjBike   CObjBikeProps
                  | CObjDog    CobjDogProbs

    data CObjPersonProps = CObjPersonProps
        { copAge :: Age
        , copGender :: Gender
        }
    …
    ```

---

### CS240h: Functional Systems in Haskell

* The following slides have been adapted (with permission) from Stanford's
  [CS240h(2011): Functional Systems in Haskell](http://www.scs.stanford.edu/11au-cs240h/) class.

* Highly recommended: Go over the
  [lecture notes](http://www.scs.stanford.edu/11au-cs240h/notes/) to
  read up on everything we couldn't cover in this course!

---

### ADTs: Interpreter

Let's create a very small fragment of a programming language:

```haskell
data Expr = Num Int             -- atom
          | Str String          -- atom
          | Op BinOp Expr Expr  -- compound
          deriving (Show)

data BinOp = Add | Concat
             deriving (Show)
```

And an interpreter for it:

```haskell
interp x@(Num _)                     = x
interp x@(Str _)                     = x
interp (Op Add a b)                  = Num (i a + i b)
  where i x = case interp x of Num a -> a
interp (Op Concat (Str a) (Str b))   = Str (a ++ b)
```

--

### ADTs: Does it work?

Our very quick round of prototyping gave us a tiny interpreter that
actually seems to work:

```haskell
>> interp (Op Add (Num 2) (Num 3))
Num 5
```

Please help me to spot some problems with my interpreter!

--

### ADTs: Two sides of the same problem

1. We can construct ill-formed expressions ("add a `Num` to a `Str`").

1. Our interpreter crashes on these expressions, because we (quite
   reasonably) didn't take their possible existence into account.


--

### ADTs: Watch your language!

Here's a slightly modified version of our language:

```haskell
data Expr a = Num Int
            | Str String
            | Op BinOp (Expr a) (Expr a)
              deriving (Show)

-- This is unchanged.
data BinOp = Add | Concat
             deriving (Show)
```

We've introduced a type parameter here...

...But we never actually use it to represent a *value* of whatever
type `a` is.


Let's see where that takes us.

---

### Phantoms: Some modifications...

Here is our modified interpreter.

```haskell
interp x@(Num _)       = x
interp x@(Str _)       = x
interp (Op Add a b)    = Num (i a + i b)
  where i x = case interp x of Num a -> a
interp (Op Concat a b) = Str (i a ++ i b)
  where i x = case interp x of Str y -> y
```

- Our only change is to apply `interp` recursively if we're asked to
  perform a `Concat`.
- We could have done this in our original interpreter, so that can't be
  the real fix.  But what *is*?
- What's the type of the rewritten `interp`?

--

### Phantoms: Our new type

The interpreter function now has this type:

```haskell
interp :: Expr a -> Expr a
```

But we know from the definitions of `Expr` and `BinOp` that we never
use a value of type `a`.  Then what purpose does this type parameter
serve?

Recall the type of `Expr`:

```haskell
data Expr a = ...
            | Op BinOp (Expr a) (Expr a)
```

--

### Phantoms: Some context

Let's think of that `a` parameter as expressing our *intent* that:

* The operands of an `Op` expression should have the same types.

* The resulting `Expr` value should *also* have this type.

```haskell
data Expr a = ...
            | Op BinOp (Expr a) (Expr a)
```

In fact, the type system will enforce these constraints for us.


--

### Phantoms: Building blocks

The first step in making all of this machinery work is to define some
functions with the right types.

These two functions will construct *atoms* in our language:

```haskell
num :: Int -> Expr Int
num = Num

str :: String -> Expr String
str = Str
```


--

### Phantoms: Applying operators safely

These two functions construct compound expressions:

```haskell
add :: Expr Int -> Expr Int -> Expr Int
add = Op Add

cat :: Expr String -> Expr String -> Expr String
cat = Op Concat
```

Notice that each one enforces the restriction that its parameters must
be compatible.


---

### Export Control:<br/> A trusted computing base

Once we have our functions defined, the last step is to lock our world
down.

Here's what the beginning of my module looks like:

```haskell
module Interp
    ( Expr,       -- type constructor
      interp,     -- interpreter
      num, str,   -- atom constructors
      add, cat,   -- expression constructors
    ) where
```

--

### Export Control

```haskell
module Interp
    ( Expr,       -- type constructor
      interp,     -- interpreter
      num, str,   -- atom constructors
      add, cat,   -- expression constructors
    ) where
```

Notice that we've exercised *careful control* over what we're
exporting.

* We export the `Expr` type constructor, but *none* of its value
  constructors.

* Users of our module don't need `BinOp`, so we don't export that at
  all.


--

### Export Control

Consequences of exporting *only* the type constructor for `Expr`:

* Clients cannot use the value constructors to create new values.
* The *only* way for a client to construct expressions is using our
  handwritten **"smart constructor"** functions.
* Clients cannot pattern-match on an `Expr` value.  Our internals are
  opaque

These are in fact standard techniques for creating abstract data types
in Haskell.  So where does the type parameter come in?

---

### Phantoms: that type parameter...

Due to our judicious use of both abstraction and that type parameter:

* Clients cannot construct ill-formed expressions. Any attempts will
  be rejected by the type checker.

This additional safety comes "for free":

* We don't need runtime checks for ill-formed expressions, because
  they cannot occur.

* Our added type parameter never represents data at runtime, so it has
  zero cost when the program runs.

--

### Phantoms: Definition

When we refer to a type parameter on the left of a type definition,
without ever using *values* of that type on the right, we call it a
*phantom type*. E.g.

```haskell
data AddExpr a = Add (AddExpr a) (AddExpr a)
```

We're essentially encoding *compile-time data* using types, and the
compiler computes with this data before our program is ever run.

---

### GADTs: Alternative Approach

Take the following `Expr` type:

```haskell
data Expr a = I Int
            | B Bool
            | Add (Expr a) (Expr a)
            | Eq  (Expr a) (Expr a)
```

…and we'd like to have:

```haskell
eval :: Expr a -> a
```

But alas, this does not work.

--

### GADTs: Alternative Approach

However, that's what **Generalized Algebraic Data Types** are for:

```haskell
data Expr a where
    I   :: Int  -> Expr Int
    B   :: Bool -> Expr Bool
    Add :: Expr Int -> Expr Int -> Expr Int
    Eq  :: Expr Int -> Expr Int -> Expr Bool

eval :: Expr a -> a
eval (I n) = n
eval (B b) = b
eval (Add e1 e2) = eval e1 + eval e2
eval (Eq  e1 e2) = eval e1 == eval e2
```

Why does this work? More about GADTs later...

---

### `IORef` Intermission

The `IORef` type, which gives us mutable references:

```haskell
import Data.IORef

newIORef    :: a -> IO (IORef a)

readIORef   :: IORef a -> IO a
writeIORef  :: IORef a -> a -> IO ()

modifyIORef :: IORef a -> (a -> a) -> IO ()
```

---

### Access Control: Managing mutation

Application writers are often faced with a question like this:

* I have a big app, and parts of it need their behaviour tweaked by an
  administrator at runtime.

There are of course many ways to address this sort of problem.

Let's consider one where we use a reference to a piece of config data.

--

### Access Control: Managing mutation

Any code that's executing in the `IO` monad can, if it knows the name of
the config reference, retrieve the current config:

```haskell
curCfg <- readIORef cfgRef
```

The trouble is, ill-behaved code could clearly also *modify* the
current configuration, and leave us with a debugging nightmare.

--

### Access Control: Phantom types...

Let's create a new type of mutable reference.

We use a phantom type `t` to statically track whether a piece of code
is allowed to modify the reference or not.

```haskell
import Data.IORef

newtype Ref t a = Ref (IORef a) -- cheap newtype!

data ReadOnly   -- no inhabitants!
data ReadWrite
```

We're already in a good spot!  Not only are we creating
compiler-enforced access control, but it will have *zero* runtime
cost.

--

### Access Control:<br/> Creating a mutable reference

To create a new reference, we just have to ensure that it has the
right type.

```haskell
newRef :: a -> IO (Ref ReadWrite a)
newRef a = Ref `fmap` newIORef a
```

--

### Access Control:<br/> Reading and writing an `Ref`

Since we want to be able to read both read-only and read-write
references, we don't need to mention the access mode when writing a
type signature for `readRef`.

```haskell
readRef :: Ref t a -> IO a
readRef (Ref ref) = readIORef ref
```

Of course, code can only write to a reference if the compiler can
statically prove (via the type system) that it has write access.

```haskell
writeRef :: Ref ReadWrite a -> a -> IO ()
writeRef (Ref ref) v = writeIORef ref v
```

--

### Access Control: Make a `Ref` read-only

This function allows us to convert any kind of reference into a
read-only reference:

```haskell
readOnly :: Ref t a -> Ref ReadOnly a
readOnly (Ref ref) = Ref ref
```

In order to prevent clients from promoting a reference from read-only
to read-write, we do *not* provide a function that goes in the
opposite direction.

Now we hide constructors:

```haskell
module Ref
    ( Ref, -- export type ctor, but not value ctor
      newRef, readOnly,
      readRef, writeRef
    ) where
```

---

### Plain lists revisited

 - Plain lists:

    ```haskell
    infixr 5 :
    data [] a = [] | a : [a]
    ```

 - ...with its single most annoying consequence:

    ```haskell
    head ∷ [a] → a
    head (x:_) = x
    head []    = error "empty list... lol, problem?"
    ```

---

### Type-level emptiness-indexed Lists

  - We can do better now that we know about `GADTs`:

    ```haskell
    {-# LANGUAGE GADTs #-}

    infixr 5 :- -- same fixity as (:)
    data List l t where
        Nil  ∷ List Empty t
        (:-) ∷ t → List l t → List NonEmpty t

    -- used as phantom types
    data Empty
    data NonEmpty
    ```

  - ...and now the emptiness of a list is known at compile time:

    ```haskell
    head' ∷ List NonEmpty t → t
    head' (x :- _) = x
    ```

--

### Type-level emptiness-indexed Lists

  - Let's define a few helpers

    ```haskell
    instance Functor (List l) where
        fmap _ Nil       = Nil
        fmap f (x :- xs) = f x :- fmap f xs
    ```

    NB: Functors are **structure-preserving maps** between
    categories hence "`l`" (i.e. the emptiness) needs to be preserved.

  - Defining a `Foldable` instance provides useful folds
    (see [`Data.Foldable` documentation](http://hackage.haskell.org/package/base/docs/Data-Foldable.html))

    ```haskell
    instance Foldable (List l) where
        foldMap _ Nil = mempty
        foldMap f (x :- xs) = f x <> foldMap f xs
    ```

--

### Type-level emptiness-indexed Lists

  - Now we can also define a `Show` instance for `List`:

    ```haskell
    instance Show t ⇒ Show (List l t) where
        show Nil = "Nil" -- suppress brackets
        show xs  = brackets .
                   intercalate " :- " .
                   (++ ["Nil"]) .  -- append "Nil" as last element
                   toList .        -- uses Foldable instance
                   fmap show $ xs  -- uses Functor instance
          where
            brackets s = "(" ++ s ++ ")"
    ```

  - Strictly speaking, the bracket handling is sub-optimal in the
    definition above. A better instance can be provided by defining the
    [`showsPrec`](http://hackage.haskell.org/package/base/docs/Prelude.html#v:showsPrec)
    method.

--

### Type-level emptiness-indexed Lists

  - We have

    ```haskell
    toList ∷ Foldable t ⇒ t a → [a]
    ```

    which allows to convert `List l t` to plain lists of type `[t]`

  - But how to get a plain list converted back to `List l t`?

--

### Type-level emptiness-indexed Lists

  - We have

    ```haskell
    toList ∷ Foldable t ⇒ t a → [a]
    ```

    which allows to convert `List l t` to plain lists of type `[t]`

  - But how to get a plain list converted back to `List l t`?

  - Here's how:

    ```haskell
    toEmpty ∷ [t] → Either (List Empty t) (List NonEmpty t)
    toEmpty []       = Left Nil
    toEmpty (x0:xs0) = Right $ go x0 xs0
      where
        go x []       = x :- Nil
        go x (x':xs') = x :- go x' xs'
    ```

    Note: We can't avoid checking `[a]` dynamically as its length is
    not known statically (or rather at the type-level).

--

### Type-level emptiness-indexed Lists

  - Let's try to implement the equivalent of `(++)` for our list type:

    ```haskell
    -- | Append two 'List' values
    (+++) ∷ List l1 t → List l2 t → List ??? t
    Nil       +++ ys = ys
    (x :- xs) +++ ys = x :- (xs +++ ys)
    ```

    What to insert for `???`?

    *(In case you'd want to cheat: GHC fails miserably at inferring the type-signature for `(+++)`)*

---

### Type-level functions

  - ```haskell
    (+++) ∷ List l1 t → List l2 t → List ??? t
    Nil       +++ ys = ys
    (x :- xs) +++ ys = x :- (xs +++ ys)
    ```

    Obviously, `???` needs be some to be a function of `l1` and `l2`,
    i.e. some kind of **type-level function**.

  - We have already seen trivial type-level functions, e.g.

    ```haskell
    type Pair a = (a,a)

    -- c.f. with the term-level function
    pair x = (x,x)
    ```

---

### Type Families

**Closed** `TypeFamilies` to the rescue:

```haskell
{-# LANGUAGE TypeFamilies #-}
type family Append e1 e2 where
    Append NonEmpty y = NonEmpty
    Append Empty    y = y

-- | Append two 'List' values
(+++) ∷ List l1 t → List l2 t → List (Append l1 l2) t
Nil       +++ ys = ys
(x :- xs) +++ ys = x :- (xs +++ ys)
```

There's also **open** type-families:

```haskell
type family Append e1 e2
type instance Append NonEmpty y = NonEmpty
type instance Append Empty    y = y
```

(Closed type-families are available only with GHC≥7.8)

Why can't we define a `Monoid` instance?

---

### Associated Types

A related concept to type-families are **associated types**:

```haskell
class ArrayElem e where
    data Array e   -- internal optimized representation for [e]
    index ∷ Array e → Word → e -- extract n-th element

instance ArrayElem Bool where
    data Array Bool = BoolArray BitVector
    index (BoolArray ar) i = indexBitVector ar i

instance ArrayElem Int where
    data Array Int = IntArray UIntArr
    index (IntArray ar) i = indexUIntArr ar i

instance (ArrayElem a, ArrayElem b) ⇒ ArrayElem (a, b) where
    data Array (a, b) = PairArray (Array a) (Array b)
    index (PairArray ar br) = (index ar i, index br i)
```

Warning: Extensive use of associated types hinders type-inference and parametricity

---

### Functional Dependencies

  - (Older) alternative to type families:

    ```haskell
    {-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies #-}
    {-# LANGUAGE FlexibleInstances, UndecidableInstances #-}

    class Add x y z | x y -> z

    instance Add Zero x x
    instance Add x y z => Add (S x) y (S z)
    ```

    Try to use type-families if possible. Especially closed type-families are easier to use.

---

### `DataKinds`

  - Back to our type-level emptiness-indexed list type.

    There's one minor weakness with respect to the `Empty`/`NonEmpty` types:

    ```haskell
    isNil ∷ List l t → Bool
    isNil Nil = True
    isNil _   = False

    -- We'd like the following to fail the type-check
    foo = isNil (undefined ∷ List () ())
    ```

    So we'd like to restrict what types the first type-variable of `List` is allowed to take.

  - Remember that slide about Kinds?

--

### `DataKinds` <small>*(recap from 1<sup>st</sup> lecture)*</small>

  * Kinds are to type expressions what types are to value expressions.

  * Haskell 98 defines the following kinds:

    - `★` - the primitive kind of nullary type constructors, e.g. `Int` or `Maybe Char`
    - `★ → ★` - the kind of unsaturated type constructors which need
      exactly one type, e.g. `Maybe` or `[]`
    - `k1 → k2` - generally, if *k1* and *k2* are kinds, then `k1 →
       k2` is the kind of types that take a type of kind *k1* and
       yield a type of kind *k2*

  * **Language extensions exist in GHC to allow to** specify "kind
    signatures", **introduce new primitive kinds beyond `★`**, or support
    kind polymorphism.

--

### `DataKinds`

  - The `DataKinds` extensions promotes term(-constructors) to types, and types to kinds.

    E.g. by enabling `DataKinds` the following declaration

    ```haskell
    data EmptyK = Empty | NonEmpty
    ```

    causes GHC to **additionally** declare the following kinds and types
    (`■`, aka "BOX", is the *sort* all kinds belong to):

    ```haskell
    kind EmptyK ∷ ■
    data 'Empty ∷ EmptyK
    data 'NonEmpty ∷ EmptyK
    ```

    Note: The `'`-prefix is optional and can be omitted if there's no ambiguity whether the promoted type is meant

--

### `DataKinds`

  - So, with the existing code, the only change needed is

    ```haskell
    {-# LANGUAGE DataKinds #-}

    data EmptyK = Empty | NonEmpty
    ```

  - And now

    ```haskell
    foo = isNil (undefined ∷ List () ())
    ```

    doesn't type-check anymore:

    ```python
    The first argument of ‘List’ should have kind ‘EmptyK’, but ‘()’ has kind ‘*’
    In an expression type signature: List () ()
    In the first argument of ‘isNil’, namely ‘(undefined ∷ List () ())’
    In the expression: isNil (undefined ∷ List () ())
    ```

    **Profit!**

---

### Type-level naturals

  - Can we define a statically checked `tail` operation on our List?

    ```haskell
    tail' ∷ List NonEmpty t → List ??? t
    ```

  - Can we define a statically checked safe indexing operation `(!!)`?

  - Let's try to generalize empty/non-empty to the length of the list...

--

### Type-level naturals


```haskell
{-# LANGUAGE DataKinds, GADTs, TypeFamilies #-}

data Nat = Z | S Nat

infixr 5 :- -- same fixity as (:)
data List l t where
  Nil  ∷ List Z t
  (:-) ∷ t → List l t → List (S l) t

head' ∷ List (S l) t → t
head' (x :- _) = x

(+++) ∷ List l1 t → List l2 t → List (Add l1 l2) t
Nil       +++ ys = ys
(x :- xs) +++ ys = x :- (xs +++ ys)

type family Add n1 n2 where
  Add Z     y  = y
  Add (S x) y  = S (Add x y)
```

--

### Type-level naturals

  - Now we can encode length-preserving operations at the type-level, e.g.

    ```haskell
    zipWith' ∷ (a → b → c) → List l a → List l b → List l c
    reverse' ∷ List l t → List l t
    ```

    Note: Our `Functor` instance encodes length-preservation at the type-level for `fmap`!

  - Now we can define a statically-checked `tail` operation:

    ```haskell
    tail' ∷ List (S l) t → List l t
    tail' (_ :- l) = l
    ```

  - ...but what about a statically checked `(!!)` indexing operation?

--

### Type-level naturals

  - We need an isomorphism between the term-level and the type-level.
    The type below is a *singleton data type* indexed by kind `Nat`:

    ```haskell
    data Nat = Z | S Nat

    data SNat n where
        SZ ∷ SNat 'Z
        SS ∷ SNat n → SNat ('S n)
    ```

  - Now we can try to write the indexing operation:

    ```haskell
    index ∷ List (S l) t → SNat i → t
    index (x :- _)    SZ    = x
    index (_ :- xs)  (SS i) = index xs i
    ```

    But this doesn't type-check yet as we need to prove that `i` is less than `S l`.

--

### Type-level naturals

  - We need another type-level helper function `:<`:

    ```haskell
    {-# LANGUAGE TypeOperators #-}

    type family n1 :< n2 where
        m     :< Z      = False
        Z     :< (S m)  = True
        (S n) :< (S m)  = n :< m

    index ∷ (i :< l) ~ True ⇒ List l t → SNat i → t
    index (x :- _)    SZ    = x
    index (_ :- xs)  (SS i) = index xs i
    ```

    Note: When compiled with GHC 7.8.2, the code above gives a (harmless) non-exhaustive pattern-match warning.

  - The binary `~` operator forces its type-level argument terms to be equal in constraint contexts.

--

### Type-level naturals

  - Btw, `GADTs` record syntax allows to have `head`/`tail` implicitly defined:

    ```haskell
    data Nat = Z | S Nat

    data List l t where
        Nil  ∷ List Z t
        Cons ∷ { lstHead ∷ t, lstTail ∷ List n t } → List (S n) t
    ```

  - In GHCi:

    ```haskell
    λ> :t lstHead
    lstHead ∷ List ('S n) t → t
    λ> :t lstTail
    lstTail ∷ List ('S n) t → List n t
    ```

--

### Type-level naturals

  - Using a peano-style representation `data Nat = Z | S Nat` is inconvenient.

  - Number literals are promoted to type-level too. The
    following works with GHC 7.8:

    ```haskell
    import GHC.TypeLits

    data List l t where
       Nil  ∷ List 0 t
       (:-) ∷ t → List l t → List (l+1) t

    head' ∷ (1<=l) ⇒ List l t → t
    head' (x :- _) = x
    ```

--

### Type-level naturals

  - The following, however, does not work (yet):

    ```haskell
    tail' ∷ List (l+1) t → List l t
    tail' (_ :- xs) = xs

    (+++) ∷ List l1 t → List l2 t → List (l1+l2) t
    Nil       +++ ys = ys
    (x :- xs) +++ ys = x :- (xs +++ ys)
    ```

--

### Type-level naturals

  - Consider the following example

    ```haskell
    dupElems :: List l t -> List (l*2) t
    dupElems Nil = Nil
    dupElems (x :- xs) = x :- x :- dupElems xs
    ```

    which results in the following compile error

    ```haskell
    Could not deduce ((l * 2) ~ (((n * 2) + 1) + 1))
    from the context (l ~ (n + 1))
    ```

    Maybe this will work in GHC 7.12

--

### Type-level naturals

  - GHC is still too weak for more serious type-literals arithmetic, the following works fine though:

    ```haskell
    {-# LANGUAGE DataKinds, KindSignatures #-}

    module ZMod where
    import GHC.TypeLits

    newtype ℤMod (n ∷ Nat) = IM Integer deriving (Eq)

    type ZMod = ℤMod -- easier to type

    instance KnownNat n ⇒ Show (ℤMod n) where
        show im@(IM x) = "(" ++ show x ++ " ∷ ℤ/" ++ (show $ natVal im) ++ ")"

    instance KnownNat n ⇒ Num (ℤMod n) where
        (IM x) * (IM y) = fromInteger (x*y)
        (IM x) + (IM y) = fromInteger (x+y)
        (IM x) - (IM y) = fromInteger (x-y)
        signum (IM x)   = fromInteger (signum x)
        abs             = id
        fromInteger i   = let im = IM (i `mod` natVal im) in im
    ```

    (c.f. [`modular-arithmetic` package](http://hackage.haskell.org/package/modular-arithmetic))


--

### Type-level naturals

  - One minor issue:

    ```haskell
    λ> (1 + 2 + 3) ∷ ZMod 0
    (*** Exception: divide by zero
    ```

  - How to catch this a compile time?

--

### Type-level naturals

One minor issue:

```haskell
λ> (1 + 2 + 3) ∷ ZMod 0
(*** Exception: divide by zero
```

How to catch this a compile time?

⇒ Simply add an additional constraint:

```haskell
instance (KnownNat n, 1<=n) ⇒ Num (ℤMod n) where
```

Now GHCi says:

```haskell
λ> (1 + 2 + 3) :: ZMod 0
<interactive>:1:8:
    Couldn't match type ‘'False’ with ‘'True’
    In the expression: (1 + 2 + 3) :: ZMod 0
    In an equation for ‘it’: it = (1 + 2 + 3) :: ZMod 0
```

---

### Statically enforced RB Tree Invariant

Example from [ICFP 2014: Depending on Types](https://www.youtube.com/watch?v=rhWMhTjQzsU):

```haskell
data Color = R | B -- NB: promoted data kind

data Tree :: Color -> Nat -> * where
  E   :: Tree B  Z
  TR  :: Tree B  n -> A -> Tree B  n -> Tree R
  TB  :: Tree c1 n -> A -> Tree c2 n -> Tree B (S n)
```

- Root is black
- Leaves are black
- Red nodes have black children
- Every path (from any node) to a leaf has the same number of black nodes

See pg. 10 in [slides](http://www.cis.upenn.edu/~sweirich/talks/icfp14.pdf)

---

### `singletons` library

allows to promote term-level functions to type-level functions (i.e. closed type-families):

```haskell
{-# LANGUAGE TemplateHaskell, QuasiQuotes, DataKinds, 
             KindSignatures, TypeFamilies, GADTs #-}

import Data.Promotion.TH

$(promote [d|

  data MyBool = MyFalse | MyTrue

  myAnd :: MyBool -> MyBool -> MyBool
  myAnd MyTrue MyTrue = MyTrue
  myAnd _      _      = MyFalse

  |])
```

---

### Dependent Type Systems

  - In Haskell,

     - values can (unsurprisingly) depend on other values,
     - values can depend on types,
     - types can depend on other types,
     - **but** types can **not** depend on values.

  - Languages such as `Agda`, `Idris`, and `Coq` are so-called
    *dependently typed* programming languages which

     - blur the distinction between type-level and term-level.
     - Specifically, they allow types to depend on values.

--

### Dependent Type Systems

  - [`Idris`](http://www.idris-lang.org/) is a *general purpose* dependently typed programming language

     - Very similiar to Haskell syntax,
     - uses eager evaluation,
     - `:` and `∷` are swapped,
     - implemented in Haskell (`.shell cabal install idris`),
     - can compile to C or JavaScript.

---

## Further Reading

* [CS240h lecture notes](http://www.scs.stanford.edu/11au-cs240h/notes/)
* [Functional Programming with Bananas, Lenses, Envelopes and Barbed Wire (1991) *Erik Meijer et al.*](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.41.125)
* [Fun with Phantom Types](http://www.cs.ox.ac.uk/ralf.hinze/publications/With.pdf)
* [Adventures With Types (video)](http://skillsmatter.com/podcast/java-jee/keynote-3842)
* [ICFP 2014: Depending on Types (video)](https://www.youtube.com/watch?v=rhWMhTjQzsU)

--

## Furter Reading *(cont.)*

* [Giving Haskell a Promotion](http://research.microsoft.com/en-us/people/dimitris/fc-kind-poly.pdf)
* [Dependently Typed Programming with Singletons](http://www.cis.upenn.edu/~eir/papers/2012/singletons/paper.pdf)
* [Promoting Functions to Type Families in Haskell](http://www.cis.upenn.edu/~eir/papers/2014/promotion/promotion.pdf)
* [The `singletons` library](http://www.cis.upenn.edu/~eir/packages/singletons/)
* [Write one program, get two (or three,or many) (video)](http://bobkonf.de/2017/loeh.html)

  </script>
</section>

	<script src="../reveal.js/lib/js/head.min.js"></script>
	<script src="../reveal.js/js/reveal.js"></script>

	<script>

		// Full list of configuration options available at:
		// https://github.com/hakimel/reveal.js#configuration
		Reveal.initialize({
			controls: true,
			progress: true,
			history: true,
			center: true,

			transition: 'slide', // none/fade/slide/convex/concave/zoom

			// Optional reveal.js plugins
			dependencies: [
				{ src: '../reveal.js/lib/js/classList.js', condition: function() { return !document.body.classList; } },
				{ src: '../reveal.js/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
				{ src: '../reveal.js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
				{ src: '../reveal.js/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
				{ src: '../reveal.js/plugin/zoom-js/zoom.js', async: true },
				{ src: '../reveal.js/plugin/notes/notes.js', async: true }
			]
		});

	</script>

</body>

</html>
