{-# LANGUAGE GADTs, DataKinds, TypeOperators, UnicodeSyntax #-}

import GHC.TypeLits

data List l t where
   Nil  ∷ List 0 t
   (:-) ∷ t → List l t → List (l+1) t

head' ∷ (1<=l) ⇒ List l t → t
head' (x :- _) = x

-- tail' ∷ List (l+1) t → List l t
-- tail' (_ :- xs) = xs


-- dupElems :: List l t -> List (l+l) t
-- dupElems Nil = Nil
-- dupElems (x :- xs) = x :- x :- dupElems xs


{-

(+++) ∷ List l1 t → List l2 t → List (l1+l2) t
Nil       +++ ys = ys
(x :- xs) +++ ys = x :- (xs +++ ys)

-}

