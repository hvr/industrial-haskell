<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
        <title>VU 183.653 - Industrial Haskell - 3rd Unit</title>


        <meta name="description" content="Lecture Notes for Industrial Haskell Course">
        <meta name="author" content="Herbert Valerio Riedel">

	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">

	<link rel="stylesheet" href="../reveal.js/css/reveal.css">
	<link rel="stylesheet" href="../reveal.js/css/theme/solarized.css" id="theme">

	<!-- Code syntax highlighting -->
	<link rel="stylesheet" href="../reveal.js/lib/css/zenburn.css">

	<!-- Printing and PDF exports -->
	<script>
		var link = document.createElement( 'link' );
		link.rel = 'stylesheet';
		link.type = 'text/css';
		link.href = window.location.search.match( /print-pdf/gi ) ? '../reveal.js/css/print/pdf.css' : '../reveal.js/css/print/paper.css';
		document.getElementsByTagName( 'head' )[0].appendChild( link );
	</script>
</head>

<body>
  <div class="reveal">
    <div class="slides">
      <section>
        <h2><a href="https://tiss.tuwien.ac.at/course/courseDetails.xhtml?courseNr=183653">VU 183.653</a></h2>

        <h2><small>Methodical,</small> <span style="color: #268bd2;">Industrial</span> <small>Software-Engineering using the</small> <span style="color: #268bd2;">Haskell</span> <small>Functional Programming Language</small></h2>
        <p>3<sup>rd</sup> Unit</p>
      </section>


<section data-markdown data-separator="^\n---\n$" data-separator-vertical="^\n--\n$">
  <script type="text/template">

## Previous lecture unit

 - GHC & Haskell Platform
 - Haskell 98/2010 & Haskell Language Report
 - WHNF & ⊥
 - Partial/Total Functions
 - User Definable Types

--

## This lecture unit

 - Kinds
 - Parametric Polymorphism
 - Typeclasses
 - Module System
 - The `IO` type & `do` syntax-sugar
 - (Extensible) exceptions
 - *Haskell kernel* sub-language

---

http://www.cs.yale.edu/publications/techreports/tr1049.pdf

> <small>In conducting the independent design review at Intermetrics, there was a signiﬁcance sense of disbelief. We quote from [CHJ93]: “It is signiﬁcant that Mr. Domanski, Mr. Banowetz and Dr. Brosgol were all surprised and suspicious when we told them that Haskell prototype P1 (see appendix B) is a complete tested executable program. We provided them with a copy of P1 without explaining that it was a program, and based on preconceptions from their past experience, they had studied P1 under the assumption that it was a mixture of requirements speciﬁcation and top level design. They were convinced it was incomplete because it did not address issues such as data structure design and execution order.</small>

---

## Kinds

 * Kinds are to type expressions what types are to value expressions.
 
 * Haskell 98 defines the following kinds:
 
    * `*` - the primitive kind of nullary type constructors, e.g. `Int` or `Maybe Char`
    * `* → *` - the kind of unsaturated type constructors which need
      exactly one type, e.g. `Maybe` or `[]`
    * *k1* `→` *k2* - generally, if *k1* and *k2* are kinds, then *k1*
       `→` *k2* is the kind of types that take a type of kind *k1* and
       yield a type of kind *k2*

--

## Kinds

 * Language extensions exist in GHC to allow to specify "kind
   signatures", introduce new primitive kinds beyond `*`, or support
   kind polymorphism.

---

## Parametric Polymorphism (reminder)

  * Can you guess what Prelude functions belong to the following type-signatures?

    ```haskell
    … :: a -> a
    … :: a -> [a]
    … :: [(a, b)] -> ([a], [b])
    … :: (a -> b -> c) -> (a, b) -> c
    … :: (a -> Bool) -> [a] -> [a]
    … :: (a -> Bool) -> [a] -> ([a], [a])
    ```

  * Try out the [Haskell prelude guessing game](http://www.sporcle.com/games/anuxerfid/haskell-prelude)

--

## Parametric Polymorphism

  * In type-signatures, lower-case identifiers are type-variables, which are implicitly ∀-quantified:

    ```haskell
    … :: ∀a.         a -> a
    … :: ∀a.         a -> [a]
    … :: ∀a. ∀b.     [(a, b)] -> ([a], [b])
    … :: ∀a. ∀b. ∀c. (a -> b -> c) -> (a, b) -> c
    … :: ∀a.         (a -> Bool) -> [a] -> [a]
    … :: ∀a.         (a -> Bool) -> [a] -> ([a], [a])
    ```

  * Such type-signatures are called "Rank 1"

--


## Parametric Polymorphism

  * Which valid implementations are there for the following type-signature?
  
    ```haskell
    foo :: (a,b) -> a
    foo = ...
    ```

  * Unconstrained type-variables provide the least possible
    information about a type, because type-variables are universally
    quantified over all concrete types.

  * Type-classes provide **ad-hoc polymorphism** and more information
    about what can be done with an instance of now constrained
    type-variable.

--

## Parametric Polymorphism

- What does this type signature mean?

   ```
   something :: a -> a
   ```

- We know that for *all possible types* `a`, this function accepts a
  value of that type, and returns a value of that type.

- We *clearly* cannot enumerate all possible types, so we equally
  clearly cannot create all (or indeed *any*) values of these types.

- Therefore, if we exclude crashes and infinite loops, the only possible
  behaviour for this function is to return its input.

--

## Parametric Polymorphism

- In fact, GHC provides a keyword, `forall` (or `∀`), to make this
  quantification over type parameters more explicit:

  ```
  something :: forall a. a -> a
  ```

- The same "universal quantification" syntax works with typeclass
  constraints:

  ```
  something :: forall a. (Show a) => a -> String
  ```

  Here, our quantifier is "for all types `a`, where the *only thing we
  know about* `a` is what the `Show` typeclass tells us we can do".

<!--  These `forall` keywords are implied if they're not explicitly written. -->

--

## Parametric Polymorphism

  * This leads to the idea of [Theorems for Free](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.38.9875)

  * [Parametricity: Money for Nothing and Theorems for Free](http://bartoszmilewski.com/2014/09/22/parametricity-money-for-nothing-and-theorems-for-free/)

--

## Parametric Polymorphism

Example from the paper:

```haskell
filter :: (a -> Bool) -> [a] -> [a]
```

Free theorem:

```haskell
filter p (map h l) ≡ map h (filter (p . h) l)
```

---

## Type-classes

* Type-classes allow to attach uniform interfaces to types
    * We humans don't cope well with complexity
    * Common structure/properties of different "things" (e.g. monoids)

* Type-classes should not be used for name-spacing
    * Bad cost/benefit: Type-signatures & errors get more noisy/complex
    * Modules provide name-spacing

--

## Type-classes

* Type-classes should have associated laws

    ```haskell
    mempty <> x   = x
    x <> mempty   = x
    x <> (y <> z) = (x <> y) <> z
    ```

--

## Type-classes

* Every Haskell programmer ought to read the [Typeclassopedia](http://www.haskell.org/haskellwiki/Typeclassopedia)

    ![](typeclassopedia.png)

--

## Type-classes

 *  Type-classes define **type-indexed dictionaries** of associated
    functions/values, thus no value is required for dispatch:

    ```haskell
    foo :: Int
    foo = maxBound
    ```

 * Haskell 98 supports automatic derivation of instances for `Eq`,
   `Ord`, `Enum`, `Bounded`, `Show`, and `Read`.

--

## Type-classes

 *  Each additional typeclass constraint gets translated into an
    implicit parameter for the typeclass dictionary, e.g. the
    following (notice the bug, btw?):
 
    ```haskell
    eqList :: (Eq a) => [a] -> [a] -> Bool
    eqList x y = and $ zipWith (==) x y
    ```
    
    gets translated into something like
    
    ```haskell
    data EqDict a = EqDict { isEq, isNEq :: a->a->Bool }
    
    eqList :: EqDict a -> [a] -> [a] -> Bool
    eqList eqDict x y = and $ zipWith (isEq eqDict) x y
    ```

    Often the compiler is able to inline and optimize the type-class
    dictionary away.

---

## Type-classes vs. OO

- Important difference from OO Classes:

```haskell
data Apple
data Banana

instance Fruit Apple
instance Fruit Banana

mkFruitCollection :: (Fruit f) => [f] -> FruitCollection
-- not valid in Haskell
```

---

## Typeclass Example: `NFData`

  * `seq` only provides WHNF reduction, i.e. "shallow" NF
 
  * Let's define an operation `deepseq` in the style of `seq` but which reduces its first argument to NF instead of just WHNF.
  
    ```haskell
    deepseq :: a -> b -> b
    ```

    Alas, there's no way to make it work in H98 with that type-signature.

  * Typeclasses to the rescue:
  
    ```haskell
    class NFData a where
        deepseq :: a -> b -> b
    ```
    
    Note: This diverges from the actual definition of the real `NFData` class

--

## Typeclass Example: `NFData`

  * Instances for primitive types can be defined in terms of `seq`:
  
    ```haskell
    instance NFData Int      where deepseq = seq
    instance NFData Integer  where deepseq = seq
    instance NFData Char     where deepseq = seq
    instance NFData Bool     where deepseq = seq
    instance NFData Double   where deepseq = seq
    instance NFData ()       where deepseq = seq
    ...
    ```

--

## Typeclass Example: `NFData`

  * And now for the instances where NF differs from WHNF:

    ```haskell
    instance NFData a => NFData (Maybe a) where
        deepseq Nothing  y = y
        deepseq (Just x) y = x `deepseq` y

    instance (NFData a, NFData b) => NFData (a,b) where
        deepseq (x1,x2) y = x1 `deepseq` x2 `deepseq` y

    instance NFData a => NFData [a] where
        deepseq []     y = y
        deepseq (x:xs) y = x `deepseq` xs `deepseq` y
    ...
    ```

--

## Typeclass Example: `NFData`

  * Syntax sugar: If we define `NFData` with a default implementation like

    ```haskell
    class NFData a where
        deepseq :: a -> b -> b
        deepseq = seq
    ```

    we can omit the "`where deepseq = seq`{.haskell}", i.e.

    ```haskell
    instance NFData Int
    instance NFData Integer
    instance NFData Char
    instance NFData Bool
    instance NFData Double
    instance NFData ()
    ...
    ```

  * API Design-choice of convenience vs. safety (why?)

---

## Module System

  * Haskell provides a module system that allows to group definitions
    and declarations into **distinct namespaces**.

  * It is possible to restrict the visible names by explicit **export
    lists**, allowing e.g. for ADTs by exporting only the type but not
    the primitive value constructors. Notable exception: Typeclass
    instances can't be hidden (unless the class is hidden as well).
    
  * Other modules can be made visible in the current module scope by
    **import** statements, which support qualified/unqalified imports,
    and/or explicitly naming the names to be imported.

  * Modules can **re-export** other modules to help flatten a huge module
    hierarchy and reduce the number of needed import statements for
    users of the library.

  * A Haskell file missing the `module` header gets prepended implicitly:
  
    ```haskell
    module Main(main) where
    ```

--

## Module System

Simple example showing some features:

```haskell
module Foo.Bar (module Foo.Bar.Doo, head, f1, f2,
                T1, T2(C1), T3(..)) where

import Foo.Bar.Doo            -- re-exported as-is
import Foo.Bar.Zoo (f1)       -- f1 is re-exported
import qualified Data.Bar.Zoo as Zoo
import Prelude hiding (head)

data T1 = HiddenConstr        -- only T1 is exported
data T2 = C1 | C2             -- only T2 and C1 is exported
data T3 = D1 | D2 | D3        -- complete type exported

f2 x = Zoo.f2 $! x

head :: [a] -> Maybe a
head []     = Nothing
head (x:_)  = Just x

```

--

## Module System

  * Convenient re-export trick (e.g. as used in [gitlib](http://hackage.haskell.org/package/gitlib)):
    
    ```haskell
    module Git (module X) where
    
    import Git.Blob as X
    import Git.Commit as X
    import Git.Commit.Push as X
    import Git.Object as X
    import Git.Reference as X
    import Git.Repository as X
    import Git.Tree as X
    import Git.Tree.Builder as X
    import Git.Types as X
    ```

  * Notice how we don't have to redundantly list each re-exported module in the export list.

---

## Impure Computations

  * So far, we've only talked about *pure* code.
  
  * "If a tree falls in a forest and no one is around to hear it, does it make a sound?"

  * We need *observable effects*, such as interacting with the world.
  
  * We need an observer: The 
    ```
    main :: IO ()
    ```
    function of the `Main` module provide the entry point.

--

## Impure Computations

  * Smallest valid trivial H98 program (but no effects):
  
    ```haskell
    main = return ()
    ```

    And here's the canonical hello-world program in H98:
    
    ```haskell
    main = putStrLn "Hello World!"
    ```

---

## `do` Notation

  * The `do` notation is just syntax sugar that gets translated to
    applications of the typeclass methods `>>`, `>>=`, `fail`, and
    some use of lambda abstractions.

--

## `do` Notation

 - The following
    
    ```haskell
    main = do putStr "What is your first name? "
              first <- getLine
              putStr "And your last name? "
              last <- getLine
              let full = first++" "++last
              putStrLn ("Pleased to meet you, "++full++"!")
    ```
    
    can be translated into
   
    ```haskell
    main = putStr "What is your first name? "  >>
           getLine                             >>= \first ->
           putStr "And your last name? "       >>
           getLine                             >>= \last  ->
           let full = first++" "++last in
           putStrLn ("Pleased to meet you, "++full++"!")
    ```       

 - Btw, "`x >> y`" is just a shortcut for "`x >>= \_ -> y`"

---

## The `IO` type

  * Notice that we didn't use the intimidating M-word yet.

  * From a practical point of view, there's nothing special about the
    `Monad` typeclass (of which `IO` happens to be an instance),
    except for Haskell providing the `do` syntax sugar.

  * The really magic thing is the `IO` type (with kind `* → *`),
    which happens to be an instance of the `Monad` typeclass.

--

## The `IO` type


  * The `IO a` type *can* be imagined as representing:
  
    ```haskell
    type IO a = World -> (a, World)
    ```

    ![](IO_a.png)

  * That strange `World` represents the state of the entire world!

--

## The `IO` type

  * Consider the two basic I/O functions:
  
    ```haskell
    getChar :: IO Char
    putChar :: Char -> IO ()
    ```

    ![](IO_char1.png)

--

## The `IO` type

  * To combine `getChar` and `putChar`, we need a sequencing **combinator**
  
    ```haskell
    (>>=) :: IO a -> (a -> IO b) -> IO b
    ```

    ![](IO_char2.png)


--

## The `IO` type

  * Figuratively speaking, `main` is provided with an initial
    `World`-state and then the runtime-system forces the evaluation of
    the final `World`-state.
    
  * A value of type `IO a` is an action that when *performed* or run
    yields some result (of type `a`) and maybe some side-effect. N.B.:
    Evaluation (i.e. via `seq`) of an "`IO a`"-*value* does not perform the
    encoded action.

--

## The `IO` type

  * This a convenient abstraction to integrate side-effects into a
    purely functional language such as Haskell with lazy evaluation.
    
  * Important property: Impure computations such as I/O have to be 
    declared explicitly at the type-level.

--

## The `IO` type

  * Forcing a value of type `IO a` to WHNF (e.g. via
    `seq`) just acts at the glueing-level (i.e. forcing the `>>=`
    composition to get evaluated).

  * There's a subtle difference of
    ["`return $! x`" vs. "`evaluate x`"](http://stackoverflow.com/questions/9553943/evaluate-function);
    consider:

    ```haskell
    -- properties of 'Control.Exception.evaluate'
    evaluate x `seq` y    ⇒  y
    evaluate x `catch` f  ⇒  (return $! x) `catch` f
    evaluate x >>= f      ⇒  (return $! x) >>= f

    f $! x = x `seq` f x
    ```

--

## The `IO` type

  * The practical difference shows up with `x=⊥`:

    ```haskell
    evaluate ⊥   `seq` y  ⇒  y
    -- vs
    return $! ⊥  `seq` y  ⇒  ⊥
    ```

---

## Exceptions

 -  H98/H2010 defines a simple exception handling infrastructure
    **in the `IO`-monad** with the following primitives:

    ```haskell
    userError :: String  -> IOError
    ioError   :: IOError -> IO a
    catch     :: IO a    -> (IOError -> IO a) -> IO a

    instance Monad IO where
        -- … bindings for return, (>>=) and (>>)
        fail s = ioError (userError s)
    ```

 -  Side-remark: [`class Monad m => MonadFail m`](https://prime.haskell.org/wiki/Libraries/Proposals/MonadFail)
           
 -  However, in state-of-the-art Haskell
    [An Extensible Dynamically-Typed Hierarchy of Exceptions](http://community.haskell.org/~simonmar/papers/ext-exceptions.pdf)
    is used.

--

## Extensible Exceptions

 -  Typeclass-based framework:

```haskell
data SomeException = ∀e . Exception e => SomeException e

class (Typeable e, Show e) => Exception e where
    toException   :: e -> SomeException
    fromException :: SomeException -> Maybe e
```

 -  New primitives such as:

```haskell
throwIO :: Exception e => e -> IO a
throw   :: Exception e => e -> a
catch   :: Exception e => IO a -> (e -> IO a) -> IO a
try     :: Exception e => IO a -> IO (Either e a)
```

--

## Extensible Exceptions

 -  `bracket` is a particularly useful abstraction:

    ```haskell
    bracket :: IO a -> (a -> IO b) -> (a -> IO c) -> IO c
    ```

 - The type `a` is for representing a resource, and therefore the
   first two arguments are for acquiring/releasing a resource, e.g.:

    ```haskell
    bracket (openFile "filename" ReadMode)
            (hClose)
            (\fileHandle -> do { … })
    ```

--

## Extensible Exceptions

 -  [Control.Exception](http://hackage.haskell.org/packages/archive/base/latest/doc/html/Control-Exception.html) provides several functions for catching exceptions. Rule of thumb from the module documentation:

     - If you want to do some cleanup in the event that an exception is raised, use `finally`, `bracket` or `onException`.
     - To recover after an exception and do something else, the best choice is to use one of the `try` family.
     - ... unless you are recovering from an asynchronous exception, in which case use `catch` or `catchJust`.

--

## Extensible Exceptions

 -  Pre-defined exception types (i.e. instances of `Exception` class):

    ```haskell
    data IOException -- abstract; IOError is type-synomym of this
    data ArithException = Overflow | Underflow | DivideByZero | …
    data AsyncException = UserInterrupt | ThreadKilled | …
    data ErrorCall = ErrorCall String -- raised by 'error' function
    …
    ```

    See [Control.Exception](http://hackage.haskell.org/packages/archive/base/latest/doc/html/Control-Exception.html) for more details.

---

## Haskell Kernel Sub-Language

Into which language subset can Haskell 98 be desugared?



* <!-- .element: class="fragment" -->  function application: `f x`
* <!-- .element: class="fragment" -->  lambda abstraction: `\x → e`
* <!-- .element: class="fragment" -->  case expression: `case x of { p1 → e1; ... }`
* <!-- .element: class="fragment" -->  simple algebraic type declarations: `data T = C1 | C2 T2`
* <!-- .element: class="fragment" -->  value bindings: `main = ...`

--

## Haskell kernel Sub-Language

* The Haskell Language Report refers to a

   - "small subset of Haskell that we call the **Haskell kernel**" which is
   - "not formally specified" but
   - "essentially a slightly sugared variant of the **lambda calculus** with
     a straightforward denotational semantics" by which
   - "translation of each syntactic structure into the kernel is given"

--

## Haskell kernel Sub-Language

For instance, the `if/else` expression

```haskell
if e1 then e2 else e3
```

is simplified into


```haskell
case e1 of { True -> e2 ; False -> e3 }
```
<!-- .element: class="fragment" -->  

--

## Haskell kernel Sub-Language

An infix function definition such as

```haskell
True || _     = True
_    || True  = True
_    || _     = False
```

can be reduced to

```haskell
(||) = \x1 x2 -> case (x1,x2) of { (True,_) -> True
                                 ; (_,True) -> True
                                 ; (_,_   ) -> False
                                 }
```

Later on, we'll discuss a similiar minimal language called "GHC Core".


  </script>
</section>

	<script src="../reveal.js/lib/js/head.min.js"></script>
	<script src="../reveal.js/js/reveal.js"></script>

	<script>

		// Full list of configuration options available at:
		// https://github.com/hakimel/reveal.js#configuration
		Reveal.initialize({
			controls: true,
			progress: true,
			history: true,
			center: true,

			transition: 'slide', // none/fade/slide/convex/concave/zoom

			// Optional reveal.js plugins
			dependencies: [
				{ src: '../reveal.js/lib/js/classList.js', condition: function() { return !document.body.classList; } },
				{ src: '../reveal.js/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
				{ src: '../reveal.js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
				{ src: '../reveal.js/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
				{ src: '../reveal.js/plugin/zoom-js/zoom.js', async: true },
				{ src: '../reveal.js/plugin/notes/notes.js', async: true }
			]
		});

	</script>

</body>

</html>
