<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
        <title>VU 183.653 [2021] - Industrial Haskell - 1st Unit</title>


        <meta name="description" content="Lecture Notes for Industrial Haskell Course">
        <meta name="author" content="Herbert Valerio Riedel">

	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">

	<link rel="stylesheet" href="../reveal.js/css/reveal.css">
	<link rel="stylesheet" href="../reveal.js/css/theme/solarized.css" id="theme">

	<!-- Code syntax highlighting -->
	<link rel="stylesheet" href="../reveal.js/lib/css/zenburn.css">

	<!-- Printing and PDF exports -->
	<script>
		var link = document.createElement( 'link' );
		link.rel = 'stylesheet';
		link.type = 'text/css';
		link.href = window.location.search.match( /print-pdf/gi ) ? '../reveal.js/css/print/pdf.css' : '../reveal.js/css/print/paper.css';
		document.getElementsByTagName( 'head' )[0].appendChild( link );
	</script>

	<style type="text/css">
	  p { text-align: left; }

	  .reveal p > code { border: 1px solid; border-radius: 0.15em; padding: 0 0.05em; font-size: 80% }
	  .reveal li > code { border: 1px solid; border-radius: 0.15em; padding: 0 0.05em; font-size: 80% }
	</style>
        
</head>

<body>
  <div class="reveal">
    <div class="slides">
      <section>
        <h2><a href="https://tiss.tuwien.ac.at/course/courseDetails.xhtml?courseNr=183653">VU 183.653</a></h2>

        <h2><small>Methodical,</small> <span style="color: #268bd2;">Industrial</span> <small>Software-Engineering using the</small> <span style="color: #268bd2;">Haskell</span> <small>Functional Programming Language</small></h2>
        <p>1<sup>st</sup> Unit | 08-03-2021 </p>
      </section>

<section data-markdown data-separator="^\n---\n$" data-separator-vertical="^\n--\n$">
  <script type="text/template">

### Haskell Implementations

 * We will use the **Glasgow Haskell Compiler** (GHC).

 * Most of the Haskell research and development is centered around
   GHC.

 * The current stable version of GHC is version 9.0.1 (released in 2021);
   The next major stable version will be 9.2.1
   
 * Stable major GHC versions are released ~~once~~twice a year

 * Information about other Haskell implementations can be found at
   [this wiki page](http://www.haskell.org/haskellwiki/Implementations)

---

### How to get Haskell installed

 * Just having a reasonably recent GHC and a `cabal` version 3.x executable is all you *need*.

 * There's multiple options:
     * For Debian/Ubuntu: https://downloads.haskell.org/debian/
     * For Windows: https://chocolatey.org/packages/haskell-dev
     * Via [Nix](https://nixos.org/)
     * Generic `rustup`-like Haskell installer that installs GHC & Cabal into your `$HOME`-folder: https://www.haskell.org/ghcup/
     * ...

---

## Editing Haskell Source 

 - You can use whatever text editor/IDE you want

 -  <!-- .element: class="fragment" --> ... **as long as it's called Emacs!** ![](real_programmers.png)

--

## Editing Haskell Source 

 * Seriously, you should use whatever text editor/IDE you are familiar with. Here
   are some popular choices (in alphabetic order):
     * Atom (with Haskell support)
     * ~~Eclipse (+ EclipseFP plugin)~~
     * Emacs (+ `haskell-mode` (+ `structured-haskell-mode`))
     * Leksah
     * Vim (+ `haskell-mode-vim`)
     * Yi
     * ...

--

## Editing Haskell Source 

 * Highly recommended features: Syntax-highlighting, indentation/alignment support

 * Nice-to-have features: Integration with GHCi REPL, tab-completion, documentation lookup

 * The majority of Haskell developers seem to prefer a
   programmer-friendly text-editor such as Emacs, Vim, or Spacemacs.

--

## Editing Haskell Source

 * A new hope:  
   https://github.com/haskell/haskell-ide-engine

---

## Compile & Run Haskell Programs

  * `ghc --make -O1 -Wall program.hs`  
    will compile a Haskell program
    into an executable `program` with additional compile-time warnings
    and a basic optimization level.
    
  * `runghc program.hs` will run the Haskell program by interpretation.

--

### Compile & Run Haskell Programs
  
  * Or you can load it up in the GHCi REPL:
  
    ```
    $ ghci ./program.hs
    GHCi, version 7.10.3: http://www.haskell.org/ghc/  :? for help
    [1 of 1] Compiling Main             ( program.hs, interpreted )
    Ok, modules loaded: Main.
    *Main> :main
    Hello World!
    *Main> _
    ```
    
    Note: if you don't see the `interpreted` keyword for a module, you
    might have a compiled module lying around.

--

### Compile & Run Haskell Programs

  * So far we didn't need to invoke `cabal`

  * We'll talk about `cabal` (& `cabal-install`) later 
    (spoiler: Cabal deals with the "package" layer of Haskell)

  * But now let's talk a bit about the Haskell language proper...

---

## Haskell Language Report

* Haskell is a language with **non-strict semantics** (however, **lazy
  evaluation** is a popular implementation method to meet the required
  semantics).

* More details: [Haskell’s Non-Strict Semantics – What Exactly does Lazy Evaluation Calculate?](https://web.archive.org/web/20170707044426/https://hackhands.com/non-strict-semantics-haskell/)

* Stated as part of the primary goals in the Preface:  
  *"It should be suitable for teaching, research, and applications, including **building large systems**."*

--

## Haskell Language Report

* Defines the basic Haskell **language**, the Haskell standard **library**,
  and the Foreign Function Interace (**FFI**).

* Pragma directive for language **extensions**

* Current version at [http://www.haskell.org/onlinereport/haskell2010](http://www.haskell.org/onlinereport/haskell2010/)

--

### Haskell 2010: What's new since H'98?

Integrates a couple of widely used language extensions:

* The Foreign Function Interface (FFI) addendum has been merged
* Hierarchical module names (e.g. `Data.List`)
* Pattern guards
* Empty datatypes (aka *uninhabited* types, e.g. `data Void`)
* `LANGUAGE` Pragma

--

### Haskell 2010: What's new since H'98?

...but also support for some misfeatures is removed/deprecated:

- The `(n+k)` pattern syntax
```haskell
factorial     0 = 1
factorial (n+1) = (n+1) * factorial n
```

- Datatype context, e.g.:
``` hs
data Eq a => Set a = Set [a]
```

--

### Haskell 2020: Outlook

 - Sad fact: Starting with version 7.10.1, GHC deviates significantly
   from good old Haskell 2010

 - Functor/Applicative/Monad/MonadFail `typeclass` refactoring

 - Many `LANGUAGE` extension pragmas have become popular, some of
   these will most likely become enabled by default in the next
   language standard
 

---

## Simple Functions

The following function already shows many Haskell features:

```haskell
filter :: (a -> Bool) -> [a] -> [a]
filter p []    = []
filter p (x:xs)
  | p x        = x : filter p xs
  | otherwise  = filter p xs
```

* <!-- .element: class="fragment" --> Polymorphic type signature
* <!-- .element: class="fragment" --> Pattern matching & guards
* <!-- .element: class="fragment" --> Currying
* <!-- .element: class="fragment" --> Recursion

---

### User-defined Fixity

 * Definable by `infixl`, `infix` and `infixr` declarations

 * Predefined fixities for standard operators:
    ``` hs
    infixl 9  !!  -- default fixity
    infixr 9  .
    infixr 8  ^, ^^, ⋆⋆
    infixl 7  ⋆, /, `quot`, `rem`, `div`, `mod`  
    infixl 6  +, -
    infixr 5  ++, :
    infix  4  ==, /=, <, <=, >=, >, `elem`, `notElem`
    infixr 3  &&
    infixr 2  ||
    infixl 1  >>, >>=
    infixr 1  =<<  
    infixr 0  $, $!, `seq`
    ``` 

 * Function application binds stronger than level 9

 * User-defined fixities can be useful in EDSL-like situations, but
   careless abuse can lead to confusing code easily.

---

## Basic Types

* Every value/expression has a type!
* Some basic types:
    * `()` - "unit" or "0-ary tuple"
    * `Bool` - boolean value
    * `Char` - Unicode code-point
    * `[Char]` - aka `String` - avoid like the plague!
    * `Int` - bounded integer
    * `Integer` - big-num integer
    * `Double` - IEEE double-precision floating-point number
    * `Int->Bool` - A function from `Int` to `Bool`

--

## Basic Types

 *  The `Bool` type definition is nothing special:

    ```haskell
    data Bool = False | True
              deriving (Eq,Ord,Read,Show,Enum,Bounded)
    ```

    But the Haskell report depends on the `Bool` type for some of its
    definitions (e.g. the `if/else` expression).

--

## Basic Types

 *  Some types can't be user defined, e.g. the list type has a special
    syntax:
   
    ```haskell
    -- not valid Haskell98
    data [] a = [] | a : [a]
              deriving (Eq,Ord,Read,Show)
    infixr 5 :
    ```

--

## Basic Types

 *  Enumeration types such as `Char` which provide literal syntax are special too:
 
    ```haskell
    -- pseudo code!
    data Char = '\NUL' | ... | ' ' | '!' | '"' | ... | 'A' | ... 
              deriving (Eq,Ord,Read,Show,Enum,Bounded)
    ```

---

## Weak Head Normal Form

  * Expressions are said to be in Weak Head Normal Form (WHNF) if the
    outermost constructor of an expression is "known".  
    In the WHNF examples below, `⊥` may represent any unevaluated
    sub-expression aka **thunk**, *including* bottom:
    
    ```haskell
    (1,True)
    True
    (⊥,⊥)
    []
    (⊥:⊥)
    [⊥,1,2,3]
    \x -> ⊥    -- the lambda-abstraction "constructor"
    filter odd -- partially applied functions
    ```    

  * Pattern-matching and `seq` are the primitive operations that can
    "force" a value into WHNF (or a divergence ⊥)

---

## ⊥ (Bottom)

  * In H'98 every type is implicitly inhabited by the special value ⊥ ("bottom").
  
  * "`⊥ :: a`" is the only value with an universal unconstrained type.
  
  * This is different from a Null-pointer: You can't test if a value is ⊥ in H98.
  
  * The value ⊥ represents error values which are technically not distinguishable from non-termination (i.e. "`let x=x in x`").

--

## ⊥ (Bottom)

  * The standard Prelude defines the following two functions for creating bottom-values on purpose:
  
    ```haskell
    error :: String -> a
    undefined :: a
    ```

  * When a ⊥ is forced to WHNF during execution the Haskell program terminates 
    (well, unless...)!

---

## Partial vs. Total Functions

  * A **total function** may only return ⊥ if its input arguments contain ⊥.
  
  * The opposite is called a **partial function**, i.e. a function which is not *defined* for all *proper* input values.

  * Some *partial* functions from the standard Prelude:
  
    ```haskell
    head :: [a] -> a
    maximum :: Ord a => [a] -> a
    (!!) :: [a] -> Int -> a
    read :: Read a => String -> a
    ```

--

## Partial vs. Total Functions

  * Non-exhaustive pattern matches are primitive source of partial functions, e.g.:
  
    ```haskell
    head :: [a] -> a
    head (x:_) = x
    ```

  * One can easily *totalize*:

    ```haskell
    head :: [a] -> Maybe a
    head (x:_) = Just x
    head []    = Nothing
    ```


--

## Partial vs. Total Functions

  * **Partial functions should be avoided whenever possible** as they
    are hidden mines waiting to blow up. 

  * However, we can't avoid partial functions completely, as
    this would restrict us to programs which are provably terminating.

---

## `seq`
 
 * The special `seq` function is defined as
 
   ```haskell
   seq :: a -> b -> b
   ⊥ `seq` b = ⊥
   a `seq` b = b
   ```

   In practice, this causes `a` to be reduced to WHNF when `seq a b`
   is reduced to WHNF as a "side-effect".

--

## `seq`

 * `seq` breaks η-conversion, i.e. it makes `\x → ⊥ x` distinguishable from `⊥`

 * Prelude also defines a `$!` (c.f. `$`) operator for simulating
   strict function application:
   ```
   ($!) :: (a -> b) -> a -> b
   f $! x  = x `seq` f x
   infixr 0 $!
   ```

---

## User Definable Types

 * Haskell 98 provides 3 ways to introduce new types:
 
    * `type` - for defining auch type synonyms
    * `data` - for defining fully fledged algebraic types, i.e. multiple constructors and fields
    * `newtype` - combination of `type` and `data`; similiar to single-constructor/field `data` type but with less overhead

--

## User Definable Types

 * Two different namespaces: Type constructors & Data constructors

 * `data` as well as `newtype` allow for **labelled fields**:
   
    ```haskell
    data C = F { f1,f2 :: Int, f3 :: Bool }
    ```

    * Labelled fields implicitly define field accessors such as `f1 :: C -> Int`
    
    * Labelled fields provide special record update syntax:
    
        ```haskell
        foo = F 1 2 True
        doo = F { f2 = 33 }  -- some fields are undefined!
        bar = foo { f2 = 22 }
        ```
        
        **Warning**: Labelled fields can lead to partial functions

---

## Strict Data Fields

  * Haskell 98 has support for making data fields *strict* by
    prepending `!` to fields, e.g.
  
    ```haskell
    Data C = F Int !Int Bool
    ```

  * The strictness annotation has only an effect for value construction.

  * The semantics for the value constructors are roughly translated into
  
    ```haskell
    F a b c = b `seq` F' a b c -- pseudo-code!
    ```

    where `F'` represents the corresponding primitive value constructor
    (without strictness).

---

## Kinds

 * Kinds are to type expressions what types are to value expressions.

--

## Kinds (Haskell 98)
 
 * `*` - the primitive kind of nullary type constructors, e.g. `Int` or `Maybe Char`

 * `* → *` - the kind of unsaturated type constructors which need
   exactly one type, e.g. `Maybe` or `[]`

 * *k1* `→` *k2* - generally, if *k1* and *k2* are kinds, then *k1*
    `→` *k2* is the kind of types that take a type of kind *k1* and
    yield a type of kind *k2*

--

## Kinds

 * Language extensions exist in GHC to allow to specify "kind
   signatures", introduce new primitive kinds beyond `*`, or support
   kind polymorphism.

---

## Parametric Polymorphism

  * Can you guess what Prelude functions belong to the following type-signatures?

    ```haskell
    … :: a -> a
    … :: a -> [a]
    … :: [(a, b)] -> ([a], [b])
    … :: (a -> b -> c) -> (a, b) -> c
    … :: (a -> Bool) -> [a] -> [a]
    … :: (a -> Bool) -> [a] -> ([a], [a])
    ```

  * In type-signatures, lower-case identifiers are type-variables, which are implicitly ∀-quantified.

  * Try out the [Haskell prelude guessing game](http://www.sporcle.com/games/anuxerfid/haskell-prelude)

--

## Parametric Polymorphism

  * Which valid implementations are there for the following type-signature?
  
    ```haskell
    foo :: (a,b) -> a
    foo = ...
    ```

  * Unconstrained type-variables provide the least possible
    information about a type, because type-variables are universally
    quantified over all concrete types.

  * Type-classes provide **ad-hoc polymorphism** and more information
    about what can be done with an instance of now constrained
    type-variable.

--

## Parametric Polymorphism

  * This leads to the idea of [Theorems for Free](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.38.9875)

  * [Parametricity: Money for Nothing and Theorems for Free](http://bartoszmilewski.com/2014/09/22/parametricity-money-for-nothing-and-theorems-for-free/)


  </script>
</section>

	<script src="../reveal.js/lib/js/head.min.js"></script>
	<script src="../reveal.js/js/reveal.js"></script>

	<script>

		// Full list of configuration options available at:
		// https://github.com/hakimel/reveal.js#configuration
		Reveal.initialize({
			controls: true,
			progress: true,
			history: true,
			center: true,

			transition: 'slide', // none/fade/slide/convex/concave/zoom

			// Optional reveal.js plugins
			dependencies: [
				{ src: '../reveal.js/lib/js/classList.js', condition: function() { return !document.body.classList; } },
				{ src: '../reveal.js/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
				{ src: '../reveal.js/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
				{ src: '../reveal.js/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
				{ src: '../reveal.js/plugin/zoom-js/zoom.js', async: true },
				{ src: '../reveal.js/plugin/notes/notes.js', async: true }
			]
		});

	</script>

</body>

</html>
